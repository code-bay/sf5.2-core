<?php

namespace CodeBay\Core\Tests\Unit\Core\Tools;

use CodeBay\Core\Arrayable;
use CodeBay\Core\Tools\ResponseDataFormatter;
use PHPUnit\Framework\TestCase;

class ResponseDataFormatterTest extends TestCase
{
    public function testFormatResponseObject() {
        $responseData = new DummyResponseData(['name' => 'john', 'email' => 'john@codebay.com']);
        $out = [];
        ResponseDataFormatter::formatResponseData($responseData, $out);
        $this->assertTrue(is_array($out));
        $this->assertEquals('john', $out['name']);
        $this->assertEquals('john@codebay.com', $out['email']);
    }

    public function testFormatResponseArray() {
        $responseData = [
            ['name' => 'john', 'email' => 'john@codebay.com'],
            ['name' => 'mich', 'email' => 'mich@codebay.com'],
        ];
        $out = [];
        ResponseDataFormatter::formatResponseData($responseData, $out);
        $this->assertCount(2, $out);
        $this->assertEquals('john', $out[0]['name']);
        $this->assertEquals('john@codebay.com', $out[0]['email']);
        $this->assertEquals('mich', $out[1]['name']);
        $this->assertEquals('mich@codebay.com', $out[1]['email']);
    }

    public function testFormatArrayOfArray() {
        $responseData = [
            new DummyResponseData(['name' => 'john', 'email' => 'john@codebay.com']),
            new DummyResponseData(['name' => 'mich', 'email' => 'mich@codebay.com']),
        ];
        $out = [];
        ResponseDataFormatter::formatResponseData($responseData, $out);
        $this->assertCount(2, $out);
        $this->assertEquals('john', $out[0]['name']);
        $this->assertEquals('john@codebay.com', $out[0]['email']);
        $this->assertEquals('mich', $out[1]['name']);
        $this->assertEquals('mich@codebay.com', $out[1]['email']);
    }

    public function testComplexNestedArray() {
        $responseData = [
            [
                new DummyResponseData(['name' => 'john', 'email' => 'john@codebay.com'])
            ],
            [
                new DummyResponseData(['name' => 'mich', 'email' => [
                    new DummyResponseData(['name' => ['michSon', 'MichGrandSon'], 'email' => 'michSon@codebay.com'])
                ]])
            ]
        ];
        $out = [];
        ResponseDataFormatter::formatResponseData($responseData, $out);
        $this->assertCount(2, $out);
        $this->assertEquals('john', $out[0][0]['name']);
        $this->assertEquals('john@codebay.com', $out[0][0]['email']);
        $this->assertEquals('mich', $out[1][0]['name']);
        $this->assertEquals('michSon', $out[1][0]['email'][0]['name'][0]);
        $this->assertEquals('MichGrandSon', $out[1][0]['email'][0]['name'][1]);
        $this->assertEquals('michSon@codebay.com', $out[1][0]['email'][0]['email']);
    }
}

class DummyResponseData extends Arrayable {
    public $name;
    public $email;
}