<?php

namespace CodeBay\Core\Tests\Unit\Core;

use CodeBay\Core\JsonMappable;
use PHPUnit\Framework\TestCase;

class Message extends JsonMappable {

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}

class Project extends JsonMappable {

    /**
     * @var Message
     */
    protected $mainMessage;

    /**
     * @var Message[]
     */
    protected $additionalMessages;

    /**
     * @return mixed
     */
    public function getMainMessage()
    {
        return $this->mainMessage;
    }

    /**
     * @return mixed
     */
    public function getAdditionalMessages()
    {
        return $this->additionalMessages;
    }

    /**
     * @param Message $mainMessage
     */
    public function setMainMessage(Message $mainMessage): void
    {
        $this->mainMessage = $mainMessage;
    }

    /**
     * @param Message[] $additionalMessages
     */
    public function setAdditionalMessages(array $additionalMessages): void
    {
        $this->additionalMessages = $additionalMessages;
    }
}

class ProjectSimpleObject extends JsonMappable {

    /**
     * @var object
     */
    protected $mainMessage;

    /**
     * @return object
     */
    public function getMainMessage()
    {
        return $this->mainMessage;
    }

    /**
     * @param object $mainMessage
     */
    public function setMainMessage($mainMessage): void
    {
        $this->mainMessage = $mainMessage;
    }
}


class ProjectSimpleArray extends JsonMappable {

    /**
     * @var array
     */
    protected $mainMessages;

    /**
     * @return array
     */
    public function getMainMessages()
    {
        return $this->mainMessages;
    }

    /**
     * @param array $mainMessages
     */
    public function setMainMessages($mainMessages): void
    {
        $this->mainMessages = $mainMessages;
    }
}

class OneOfClass extends JsonMappable {

    /**
     * @var ProjectSimpleObject|Message
     */
    public $attr;

    /**
     * @param ProjectSimpleObject|Message $attr
     */
    public function setAttr($attr)
    {
        $this->attr = $attr;
    }

    /**
     * @return Message|ProjectSimpleObject
     */
    public function getAttr()
    {
        return $this->attr;
    }
}

class JsonMappableTest extends TestCase
{
    public function testNestedBuild() {
        $payload = [
            'mainMessage' => [
                'id' => 1,
                'name' => 'Greetings',
            ],
            'additionalMessages' => [
                [
                    'id' => 1,
                    'name' => 'test'
                ]
            ]
        ];
        $project = new Project($payload);
        $this->assertTrue($project instanceof Project);
        $this->assertTrue($project->getMainMessage() instanceof Message);
    }

    public function testMapSimpleObject() {
        $payload = [
            'mainMessage' => [
                'id' => 1,
                'name' => 'Greetings',
            ],
        ];
        $project = new ProjectSimpleObject($payload);
        $this->assertTrue($project instanceof ProjectSimpleObject);
        $this->assertEquals(1, $project->getMainMessage()->id);
        $this->assertEquals('Greetings', $project->getMainMessage()->name);
    }

    public function testMapSimpleArray() {
        $payload = [
            'mainMessages' => [
                [
                    'id' => 1,
                    'name' => 'Greetings',
                ],
                [
                    'id' => 2,
                    'name' => 'Hello',
                ]
            ],
        ];
        $project = new ProjectSimpleArray($payload);
        $this->assertTrue($project instanceof ProjectSimpleArray);
        $this->assertIsArray($project->getMainMessages());
        $this->assertCount(2, $project->getMainMessages());
        $first = $project->getMainMessages()[0];
        $this->assertEquals(1, $first->id);
        $this->assertEquals('Greetings', $first->name);
        $second = $project->getMainMessages()[1];
        $this->assertEquals(2, $second->id);
        $this->assertEquals('Hello', $second->name);
    }
}