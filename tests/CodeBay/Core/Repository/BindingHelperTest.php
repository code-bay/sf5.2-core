<?php

namespace CodeBay\Core\Tests\Unit\Core\Repository;

use CodeBay\Core\Repository\BindingHelper;
use PHPUnit\Framework\TestCase;

class BindingHelperTest extends TestCase
{
    public function testGetBindingValues() {
        $values = [2, 4, 8, 16];
        $bindingValues = BindingHelper::getBindingValues($values);
        $this->assertEquals(['v_0' => 2, 'v_1' => 4, 'v_2' => 8, 'v_3' => 16], $bindingValues);

        $values = [];
        $bindingValues = BindingHelper::getBindingValues($values);
        $this->assertCount(0, $bindingValues);

        $values = 5;
        $bindingValues = BindingHelper::getBindingValues($values);
        $this->assertCount(0, $bindingValues);

        $values = [2, 4, 8, 16];
        $bindingValues = BindingHelper::getBindingValues($values, '');
        $this->assertCount(0, $bindingValues);

        $values = [2, 4, 8, 16];
        $bindingValues = BindingHelper::getBindingValues($values, 'pref');
        $this->assertEquals(['pref_0' => 2, 'pref_1' => 4, 'pref_2' => 8, 'pref_3' => 16], $bindingValues);
    }

    public function testGetBindingKeys() {
        $values = [2, 4, 8, 16];
        $bindingKeys = BindingHelper::getBindingKeys($values);
        $this->assertEquals([':v_0', ':v_1', ':v_2', ':v_3'], $bindingKeys);

        $values = [];
        $bindingKeys = BindingHelper::getBindingKeys($values);
        $this->assertCount(0, $bindingKeys);

        $values = 5;
        $bindingKeys = BindingHelper::getBindingKeys($values);
        $this->assertCount(0, $bindingKeys);

        $values = [2, 4, 8, 16];
        $bindingKeys = BindingHelper::getBindingKeys($values, '');
        $this->assertCount(0, $bindingKeys);

        $values = [2, 4, 8, 16];
        $bindingKeys = BindingHelper::getBindingKeys($values, 'pref');
        $this->assertEquals([':pref_0', ':pref_1', ':pref_2', ':pref_3'], $bindingKeys);
    }

    public function testGetInBindingString() {
        $values = [2, 4, 8, 16];
        $bindingString = BindingHelper::getInBindingString($values);
        $this->assertEquals(':v_0,:v_1,:v_2,:v_3', $bindingString);

        $values = [];
        $bindingString = BindingHelper::getInBindingString($values);
        $this->assertNull($bindingString);

        $values = 5;
        $bindingString = BindingHelper::getInBindingString($values);
        $this->assertNull($bindingString);

        $values = [2, 4, 8, 16];
        $bindingString = BindingHelper::getInBindingString($values, '');
        $this->assertNull($bindingString);

        $values = [2, 4, 8, 16];
        $bindingString = BindingHelper::getInBindingString($values, 'pref');
        $this->assertEquals(':pref_0,:pref_1,:pref_2,:pref_3', $bindingString);
    }
}
