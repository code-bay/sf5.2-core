<?php

namespace CodeBay\Core\Tests\Unit\Core\Service;

use CodeBay\Core\Model\StructurePath;
use CodeBay\Core\Service\ArrayTools;
use PHPUnit\Framework\TestCase;


class ArrayToolsTest extends TestCase
{

    public function testSetValueAtPathRoot()
    {

        $input = [];
        $path = new StructurePath('#');
        $value = 3;

        ArrayTools::setValueAtPath($input, $path, $value);

        $this->assertEquals(3, $input);
    }

    public function testSetValueAtPathNonExistingPath()
    {

        $input = [];
        $path = new StructurePath('#.tickets.mailId');
        $value = 3;

        ArrayTools::setValueAtPath($input, $path, $value);

        $this->assertArrayHasKey('tickets', $input);
        $this->assertArrayHasKey('mailId', $input['tickets']);
        $this->assertEquals(3, $input['tickets']['mailId']);
    }

    public function testSetValueAtPathPartialExistingPath()
    {
        $input = [
            'tickets' => []
        ];
        $path = new StructurePath('#.tickets.mailId');
        $value = 3;

        ArrayTools::setValueAtPath($input, $path, $value);

        $this->assertArrayHasKey('tickets', $input);
        $this->assertArrayHasKey('mailId', $input['tickets']);
        $this->assertEquals(3, $input['tickets']['mailId']);
    }

    public function testSetValueAtPathFullExistingPath()
    {
        $input = [
            'tickets' => [
                'mailId' => [
                ]
            ]
        ];
        $path = new StructurePath('#.tickets.mailId');
        $value = 3;

        ArrayTools::setValueAtPath($input, $path, $value);

        $this->assertArrayHasKey('tickets', $input);
        $this->assertArrayHasKey('mailId', $input['tickets']);
        $this->assertEquals(3, $input['tickets']['mailId']);
    }

    public function testSetValueAtPathComplexOut()
    {
        $input = [
            'tickets' => [],
            'batchId' => 10
        ];
        $path = new StructurePath('#.tickets.alerts');
        $value = [
            [
                'id' => 1
            ],
            [
                'id' => 2
            ]
        ];

        ArrayTools::setValueAtPath($input, $path, $value);

        $this->assertArrayHasKey('tickets', $input);
        $this->assertArrayHasKey('batchId', $input);
        $this->assertEquals(10, $input['batchId']);
        $this->assertArrayHasKey('alerts', $input['tickets']);
        $this->assertEquals(1, $input['tickets']['alerts'][0]['id']);
        $this->assertEquals(2, $input['tickets']['alerts'][1]['id']);
    }

    public function testGetValueAtPath()
    {
        $input = [
            'tickets' => [],
            'batchId' => 10
        ];
        $path = new StructurePath('#.tickets.alerts');
        $valueAtPath = ArrayTools::getValueAtPath($input, $path);
        $this->assertNull($valueAtPath);
    }

    public function testGetValueAtPathRoot()
    {
        $input = [
            [
                "ticketId" => 456,
                "mailId" => 1,
                "expeditor" => "Mic"
            ],
            [
                "ticketId" => 74,
                "mailId" => 8,
                "expeditor" => "Jon"
            ],
            [
                "ticketId" => 4447,
                "mailId" => 16,
                "expeditor" => "Cla"
            ]
        ];
        $path = new StructurePath('#');
        $valueAtPath = ArrayTools::getValueAtPath($input, $path);
        $this->assertCount(3, $valueAtPath);
        $this->assertEquals(1, $valueAtPath[0]['mailId']);
    }
}