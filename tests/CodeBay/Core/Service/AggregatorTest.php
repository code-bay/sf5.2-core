<?php

namespace CodeBay\Core\Tests\Unit\Core\Service;

use CodeBay\Core\JsonMappable;
use CodeBay\Core\Service\Aggregator;
use PHPUnit\Framework\TestCase;

class TicketSqlResult extends JsonMappable {
    public $ticketId;
    public $mailId;
    public $expeditor;
    public $file;
    public $fileExists;
}


/**
 * Class Ticket
 * @package CodeBay\Core\Tests\Unit\Core\Service
 * @jsonSchema --{"definitions":{"Reupload":{"type":"object","properties":{"file":{"type":"string"}}},"Sender":{"type":"object","properties":{"expeditor":{"type":"string"},"alerts":{"type":"array","items":{"$ref":"#\/definitions\/Alert"}}}},"Alert":{"type":"object","properties":{"file":{"type":"string"},"mailId":{"type":"string"}}}},"type":"object","properties":{"id":{"type":"number"},"reuploads":{"type":"array","items":{"$ref":"#\/definitions\/Reupload"}},"senders":{"type":"array","items":{"$ref":"#\/definitions\/Sender"}}}}
 * @definitionNamespace CodeBay\Core\Tests\Unit\Core\Service
 */
class Ticket extends JsonMappable
{
    /**
     * @var float
     */
    public $id;

    /**
     * @var \CodeBay\Core\Tests\Unit\Core\Service\Reupload[]
     */
    public $reuploads;

    /**
     * @var \CodeBay\Core\Tests\Unit\Core\Service\Sender[]
     */
    public $senders;
}

/**
 * Class Sender
 * @package CodeBay\Core\Tests\Unit\Core\Service
 * @jsonSchema --{"definitions":{"Alert":{"type":"object","properties":{"file":{"type":"string"},"mailId":{"type":"string"}}}},"type":"object","properties":{"expeditor":{"type":"string"},"alerts":{"type":"array","items":{"$ref":"#\/definitions\/Alert"}}}}
 * @definitionNamespace CodeBay\Core\Tests\Unit\Core\Service
 */
class Sender extends JsonMappable
{
    /**
     * @var string
     */
    public $expeditor;

    /**
     * @var \CodeBay\Core\Tests\Unit\Core\Service\Alert[]
     */
    public $alerts;
}

/**
 * Class Reupload
 * @package CodeBay\Core\Tests\Unit\Core\Service
 * @jsonSchema --{"type":"object","properties":{"file":{"type":"string"}}}
 * @definitionNamespace CodeBay\Core\Tests\Unit\Core\Service
 */
class Reupload extends JsonMappable
{
    /**
     * @var string
     */
    public $file;
}

/**
 * Class Alert
 * @package CodeBay\Core\Tests\Unit\Core\Service
 * @jsonSchema --{"type":"object","properties":{"file":{"type":"string"},"mailId":{"type":"string"}}}
 * @definitionNamespace CodeBay\Core\Tests\Unit\Core\Service
 */
class Alert extends JsonMappable
{
    /**
     * @var string
     */
    public $file;

    /**
     * @var string
     */
    public $mailId;
}

/**
 * Class Analysis
 * @package CodeBay\Core\Tests\Unit\Core\Service
 * @jsonSchema --{"definitions":{"Ticket":{"type":"object","properties":{"id":{"type":"number"},"reuploads":{"type":"array","items":{"$ref":"#\/definitions\/Reupload"}},"senders":{"type":"array","items":{"$ref":"#\/definitions\/Sender"}}}},"Reupload":{"type":"object","properties":{"file":{"type":"string"}}},"Sender":{"type":"object","properties":{"expeditor":{"type":"string"},"alerts":{"type":"array","items":{"$ref":"#\/definitions\/Alert"}}}},"Alert":{"type":"object","properties":{"file":{"type":"string"},"mailId":{"type":"string"}}}},"type":"object","properties":{"tickets":{"type":"array","items":{"$ref":"#\/definitions\/Ticket"}}}}
 * @definitionNamespace CodeBay\Core\Tests\Unit\Core\Service
 */
class Analysis extends JsonMappable
{
    /**
     * @var \CodeBay\Core\Tests\Unit\Core\Service\Ticket[]
     */
    public $tickets;
}


/**
 * Class Auth
 * @package CodeBay\Core\Tests\Unit\Core\Service
 * @jsonSchema --{"type":"object","properties":{"token":{"type":"string"}}}
 * @definitionNamespace CodeBay\Core\Tests\Unit\Core\Service
 */
class Auth extends JsonMappable
{
    /**
     * @var string
     */
    public $token;
}

/**
 * Class ResponseObject
 * @package CodeBay\Core\Tests\Unit\Core\Service
 * @jsonSchema --{"definitions":{"Auth":{"type":"object","properties":{"token":{"type":"string"}}},"Ticket":{"type":"object","properties":{"id":{"type":"number"},"reuploads":{"type":"array","items":{"$ref":"#\/definitions\/Reupload"}},"senders":{"type":"array","items":{"$ref":"#\/definitions\/Sender"}}}},"Reupload":{"type":"object","properties":{"file":{"type":"string"}}},"Sender":{"type":"object","properties":{"expeditor":{"type":"string"},"alerts":{"type":"array","items":{"$ref":"#\/definitions\/Alert"}}}},"Alert":{"type":"object","properties":{"file":{"type":"string"},"mailId":{"type":"string"}}}},"type":"object","properties":{"id": {"type": "string"}, "auth": {"$ref":"#\/definitions\/Auth"}, "alerts":{"type":"array","items":{"$ref":"#\/definitions\/Ticket"}}}}
 * @definitionNamespace CodeBay\Core\Tests\Unit\Core\Service
 */
class ResponseObject extends JsonMappable
{
    /**
     * @var \CodeBay\Core\Tests\Unit\Core\Service\Ticket[]
     */
    public $alerts;

    /**
     * @var string
     */
    public $id;

    /**
     * @var \CodeBay\Core\Tests\Unit\Core\Service\Auth
     */
    public $auth;

}

/**
 * Class TicketResponseItem
 * @package CodeBay\Core\Tests\Unit\Core\Service
 * @jsonSchema --{"type":"object","properties":{"ticketId":{"type":"string"}, "mailId":{"type":"string"}, "expeditor":{"type":"string"}}}
 * @definitionNamespace CodeBay\Core\Tests\Unit\Core\Service
 */
class TicketResponseItem extends JsonMappable
{
    /**
     * @var string
     */
    public $ticketId;
    /**
     * @var string
     */
    public $mailId;
    /**
     * @var string
     */
    public $expeditor;
}



class AggregatorTest extends TestCase
{
    /**
     * @var TicketSqlResult[]
     */
    private $inputData;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        $this->inputData = [];
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 123,
            'mailId' => 12,
            'expeditor' => 'JL',
            'file' => '1.pdf',
            'fileExists' => true,
        ]);
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 123,
            'mailId' => 12,
            'expeditor' => 'JL',
            'file' => '2.pdf',
            'fileExists' => false,
        ]);
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 456,
            'mailId' => 32,
            'expeditor' => 'Mic',
            'file' => 'u.txt',
            'fileExists' => true,
        ]);
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 456,
            'mailId' => 33,
            'expeditor' => 'Jul',
            'file' => 's.txt',
            'fileExists' => false,
        ]);
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 456,
            'mailId' => 33,
            'expeditor' => 'Jul',
            'file' => 'v.txt',
            'fileExists' => false,
        ]);
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 456,
            'mailId' => 33,
            'expeditor' => 'HUG',
            'file' => 'j.pdf',
            'fileExists' => false,
        ]);
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 88,
            'mailId' => 40,
            'expeditor' => 'Lau',
            'file' => '2.pdf',
            'fileExists' => true,
        ]);
    }

    public function testGroup() {

        $groupDefinitions = [
            'name' => 'tickets',
            'criteria' => 'ticketId',
            'fields' => [
                [ 'src' => 'ticketId', 'dst' => 'id' ]
            ],
            'children' => [
                [
                    'name' => 'reuploads',
                    'criteria' => null,
                    'fields' => [
                        [ 'src' => 'file' ]
                    ],
                    'filters' => [
                        ['src' => 'fileExists', 'operation' => 'IS_TRUE']
                    ],
                    'children' => [],
                ],
                [
                    'name' => 'senders',
                    'criteria' => 'expeditor',
                    'fields' => [
                        [ 'src' => 'expeditor' ]
                    ],
                    'filters' => [
                        ['src' => 'fileExists', 'operation' => 'IS_FALSE']
                    ],
                    'children' => [
                        [
                            'name' => 'alerts',
                            'criteria' => null,
                            'fields' => [
                                [ 'src' => 'mailId' ],
                                [ 'src' => 'file' ]
                            ],
                            'children' => [],
                        ]
                    ]
                ],
            ]
        ];

        $targetJsonInput = [
            'tickets' => [
                [
                    'id' => 123,
                    'reuploads' => [
                        [
                            'file' => '1.pdf'
                        ]
                    ],
                    'senders' => [
                        [
                            'expeditor' => 'JL',
                            'alerts' => [
                                [
                                    'file' => '2.pdf',
                                    'mailId' => 12
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    'id' => 456,
                    'reuploads' => [
                        [
                            'file' => 'u.txt'
                        ]
                    ],
                    'senders' => [
                        [
                            'expeditor' => 'Jul',
                            'alerts' => [
                                [
                                    'file' => 's.txt',
                                    'mailId' => 33
                                ],
                                [
                                    'file' => 'v.txt',
                                    'mailId' => 33
                                ]
                            ]
                        ],
                        [
                            'expeditor' => 'HUG',
                            'alerts' => [
                                [
                                    'file' => 'j.pdf',
                                    'mailId' => 33
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    'id' => 88,
                    'reuploads' => [
                        [
                            'file' => '2.pdf'
                        ]
                    ],
                    'senders' => []
                ],
            ]
        ];

        $inputsAsArray = [];
        foreach ($this->inputData as $inputDatum) {
            $inputsAsArray[] = $inputDatum->toArray();
        }
        $aggregator = new Aggregator($groupDefinitions, $inputsAsArray);
        $output = $aggregator->aggregate();

        $this->assertArrayHasKey('tickets', $output);

        $analysis = new Analysis($output);

        $analysisAsArray = $analysis->toArray();
        $this->assertEquals($analysisAsArray, $targetJsonInput);
    }


}