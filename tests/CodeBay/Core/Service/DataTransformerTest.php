<?php

namespace CodeBay\Core\Tests\Unit\Core\Service;

use CodeBay\Core\Model\StructurePath;
use CodeBay\Core\Model\TransformInput;
use CodeBay\Core\Model\TransformSpec;
use CodeBay\Core\Service\DataTransformer;
use PHPUnit\Framework\TestCase;


/**
 * Class DummyEntity
 * @package CodeBay\Hiram\Tests\Unit\CbApiGeneratorBundle\Service
 * @Entity
 * @Table(name="dummy")
 */
class DummyEntity
{
    /**
     * @var integer
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="SEQUENCE")
     */
    protected $id;

    /**
     * @var string
     * @Column(type="date",name="creationDate",unique=false,nullable=true)
     */
    protected $token;

    /**
     * @var \DateTime
     * @Column(type="datetime",name="modification_date",unique=false,nullable=true)
     */
    public $modificationDate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return \DateTime
     */
    public function getModificationDate(): \DateTime
    {
        return $this->modificationDate;
    }

    /**
     * @param \DateTime $modificationDate
     */
    public function setModificationDate(\DateTime $modificationDate): void
    {
        $this->modificationDate = $modificationDate;
    }
}

class DataTransformerTest extends TestCase
{
    /**
     * @var TicketSqlResult[]
     */
    private $inputData;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        $this->inputData = [];
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 123,
            'mailId' => 12,
            'expeditor' => 'JL',
        ]);
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 123,
            'mailId' => 12,
            'expeditor' => 'JL',
        ]);
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 456,
            'mailId' => 32,
            'expeditor' => 'Mic',
        ]);
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 456,
            'mailId' => 33,
            'expeditor' => 'Jul',
        ]);
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 456,
            'mailId' => 33,
            'expeditor' => 'Jul',
        ]);
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 456,
            'mailId' => 33,
            'expeditor' => 'HUG',
        ]);
        $this->inputData[] = new TicketSqlResult([
            'ticketId' => 88,
            'mailId' => 40,
            'expeditor' => 'Lau',
        ]);
    }

    public function testTransformScalar() {

        $ts = new TransformSpec(
            new StructurePath('#'), new StructurePath('#.mailId'), '1', []
        );

        $inputs = [
            new TransformInput('1', 'testId')
        ];
        $specs = [
            $ts
        ];

        $transformed = DataTransformer::transform($inputs, $specs);

        $this->assertArrayHasKey('mailId', $transformed);
        $this->assertEquals('testId', $transformed['mailId']);
    }

    public function testTransformComplex() {
        $runId = 14575;
        $bearer = 'HWoisefk';
        $inputDataAsArray = [];
        foreach ($this->inputData as $inputDatum) {
            $inputDataAsArray[] = $inputDatum->toArray();
        }
        $resultsDef = [
            'batchId' => 1,
            'timestamp' => 190837629,
            'outputs' => $inputDataAsArray
        ];

        $tsRunId = new TransformSpec(
            new StructurePath('#'), new StructurePath('#.id'), 1, []
        );
        $tsBearer = new TransformSpec(
            new StructurePath('#'), new StructurePath('#.auth.token'), 2, []
        );
        $tsAlerts = new TransformSpec(
            new StructurePath('#.outputs'), new StructurePath('#.alerts'), 3, [
                'name' => 'tickets',
                'criteria' => 'ticketId',
                'fields' => [
                    [ 'src' => 'ticketId' ]
                ],
                'children' => [
                    [
                        'name' => 'senders',
                        'criteria' => 'expeditor',
                        'fields' => [
                            [ 'src' => 'expeditor' ]
                        ],
                        'children' => []
                    ]
                ],
            ]
        );

        $inputs = [
            new TransformInput(1, $runId),
            new TransformInput(2, $bearer),
            new TransformInput(3, $resultsDef),
        ];
        $specs = [
            $tsRunId, $tsBearer, $tsAlerts
        ];

        $transformed = DataTransformer::transform($inputs, $specs);

        $this->assertArrayHasKey('id', $transformed);
        $this->assertEquals(14575, $transformed['id']);
        $this->assertArrayHasKey('auth', $transformed);
        $this->assertArrayHasKey('token', $transformed['auth']);
        $this->assertEquals('HWoisefk', $transformed['auth']['token']);
        $this->assertArrayHasKey('alerts', $transformed);
        $this->assertArrayHasKey('tickets', $transformed['alerts']);
        $this->assertEquals(123, $transformed['alerts']['tickets'][0]['ticketId']);
        $this->assertEquals(456, $transformed['alerts']['tickets'][1]['ticketId']);
        $this->assertEquals(88, $transformed['alerts']['tickets'][2]['ticketId']);
        $this->assertCount(3, $transformed['alerts']['tickets']);
        $this->assertCount(1, $transformed['alerts']['tickets'][0]['senders']);
        $this->assertCount(3, $transformed['alerts']['tickets'][1]['senders']);
    }

    public function testHydrateOutputJsonMappable() {

        $runId = 14575;
        $bearer = 'HWoisefk';
        $inputDataAsArray = [];
        foreach ($this->inputData as $inputDatum) {
            $inputDataAsArray[] = $inputDatum->toArray();
        }
        $resultsDef = [
            'batchId' => 1,
            'timestamp' => 190837629,
            'outputs' => $inputDataAsArray
        ];

        $tsRunId = new TransformSpec(
            new StructurePath('#'), new StructurePath('#.id'), 1, []
        );
        $tsBearer = new TransformSpec(
            new StructurePath('#'), new StructurePath('#.auth.token'), 2, []
        );
        $tsAlerts = new TransformSpec(
            new StructurePath('#.outputs'), new StructurePath('#'), 3, [
                'name' => 'alerts',
                'criteria' => 'ticketId',
                'fields' => [
                    [ 'src' => 'ticketId', 'dst' => 'id' ]
                ],
                'children' => [
                    [
                        'name' => 'senders',
                        'criteria' => 'expeditor',
                        'fields' => [
                            [ 'src' => 'expeditor' ]
                        ],
                        'children' => []
                    ]
                ],
            ]
        );

        $inputs = [
            new TransformInput(1, $runId),
            new TransformInput(2, $bearer),
            new TransformInput(3, $resultsDef),
        ];
        $specs = [
            $tsRunId, $tsBearer, $tsAlerts
        ];
        $transformed = DataTransformer::transform($inputs, $specs);
        /** @var ResponseObject $output */
        $output = DataTransformer::hydrateOutput(ResponseObject::class, $transformed, true);
        $this->assertInstanceOf(ResponseObject::class, $output);

        $this->assertEquals(14575, $output->id);
        $this->assertEquals('HWoisefk', $output->auth->token);
        $this->assertEquals(123, $output->alerts[0]->id);
        $this->assertEquals(456, $output->alerts[1]->id);
        $this->assertEquals(88, $output->alerts[2]->id);
        $this->assertEquals('Mic', $output->alerts[1]->senders[0]->expeditor);
        $this->assertEquals('Jul', $output->alerts[1]->senders[1]->expeditor);
        $this->assertEquals('Lau', $output->alerts[2]->senders[0]->expeditor);
    }

    public function testHydrateOutputEntity() {
        $runId = 14575;
        $bearer = 'HWoisefk';
        $tsRunId = new TransformSpec(
            new StructurePath('#'), new StructurePath('#.id'), 1, []
        );
        $tsBearer = new TransformSpec(
            new StructurePath('#'), new StructurePath('#.token'), 2, []
        );
        $inputs = [
            new TransformInput(1, $runId),
            new TransformInput(2, $bearer),
        ];
        $specs = [
            $tsRunId, $tsBearer
        ];
        $transformed = DataTransformer::transform($inputs, $specs);
        /** @var DummyEntity $output */
        $output = DataTransformer::hydrateOutput(DummyEntity::class, $transformed, false);
        $this->assertEquals(14575, $output->getId());
        $this->assertEquals('HWoisefk', $output->getToken());
    }

    public function testHydrateOutputEntityCollection() {
        $inputDataAsArray = [];
        foreach ($this->inputData as $inputDatum) {
            $inputDataAsArray[] = $inputDatum->toArray();
        }
        $resultsDef = [
            'outputs' => $inputDataAsArray
        ];
        $tsAlerts = new TransformSpec(
            new StructurePath('#.outputs'), new StructurePath('#'), 1, [
                'name' => 'alerts',
                'isRoot' => true,
                'criteria' => null,
                'fields' => [
                    [ 'src' => 'ticketId', 'dst' => 'id' ],
                    [ 'src' => 'expeditor', 'dst' => 'token' ],
                ],
                'children' => [],
            ]
        );
        $inputs = [
            new TransformInput(1, $resultsDef),
        ];
        $specs = [
            $tsAlerts
        ];
        $transformed = DataTransformer::transform($inputs, $specs);

        /** @var DummyEntity[] $output */
        $output = DataTransformer::hydrateOutput(DummyEntity::class, $transformed, false, true);
        $this->assertIsArray($output);
        $this->assertCount(7, $output);
        $this->assertInstanceOf(DummyEntity::class, $output[0]);
    }

    public function testHydrateOutputScalar() {
        $runId = 14575;
        $tsRunId = new TransformSpec(
            new StructurePath('#'), new StructurePath('#'), 1, []
        );
        $inputs = [
            new TransformInput(1, $runId),
        ];
        $specs = [
            $tsRunId
        ];
        $transformed = DataTransformer::transform($inputs, $specs);
        /** @var DummyEntity $output */
        $output = DataTransformer::hydrateOutput(null, $transformed, false);
        $this->assertEquals(14575, $output);
    }

    public function testHydrateOutputScalarCollection() {
        $inputDataAsArray = [];
        foreach ($this->inputData as $inputDatum) {
            $inputDataAsArray[] = $inputDatum->toArray();
        }
        $resultsDef = [
            'outputs' => $inputDataAsArray
        ];
        $tsAlerts = new TransformSpec(
            new StructurePath('#.outputs'), new StructurePath('#'), 1, [
                'name' => 'alerts',
                'isRoot' => true,
                'criteria' => null,
                'fields' => [
                    [ 'src' => 'ticketId']
                ],
                'children' => [],
            ]
        );
        $inputs = [
            new TransformInput(1, $resultsDef),
        ];
        $specs = [
            $tsAlerts
        ];
        $transformed = DataTransformer::transform($inputs, $specs);

        $output = DataTransformer::hydrateOutput(null, $transformed, false, true);
        $this->assertIsArray($output);
        $this->assertCount(7, $output);
        $this->assertEquals(123, $output[0]);
    }


    public function testHydrateOutputEntityTypes() {
        $runId = 14575;
        $bearer = 'HWoisefk';
        $dateTime = '2022-01-05 10:15:57';
        $tsRunId = new TransformSpec(
            new StructurePath('#'), new StructurePath('#.id'), 1, []
        );
        $tsBearer = new TransformSpec(
            new StructurePath('#'), new StructurePath('#.token'), 2, []
        );
        $tsDateTime = new TransformSpec(
            new StructurePath('#'), new StructurePath('#.modificationDate'), 3, []
        );
        $inputs = [
            new TransformInput(1, $runId),
            new TransformInput(2, $bearer),
            new TransformInput(3, $dateTime),
        ];
        $specs = [
            $tsRunId, $tsBearer, $tsDateTime
        ];
        $transformed = DataTransformer::transform($inputs, $specs);
        /** @var DummyEntity $output */
        $output = DataTransformer::hydrateOutput(DummyEntity::class, $transformed, false);
        $this->assertEquals(14575, $output->getId());
        $this->assertEquals('HWoisefk', $output->getToken());
        $this->assertInstanceOf(\DateTime::class, $output->getModificationDate());
        $this->assertEquals('2022-01-05 10:15:57', $output->getModificationDate()->format('Y-m-d H:i:s'));
    }

    public function testHydrateOutputArrayOfObjectRefs() {
        $inputDataAsArray = [];
        foreach ($this->inputData as $inputDatum) {
            $inputDataAsArray[] = $inputDatum->toArray();
        }
        $resultsDef = $inputDataAsArray;
        $tsAlerts = new TransformSpec(
            new StructurePath('#'), new StructurePath('#'), 1, [
                'name' => 'items',
                'isRoot' => true,
                'criteria' => null,
                'fields' => [
                    [ 'src' => 'ticketId'],
                    [ 'src' => 'mailId'],
                    [ 'src' => 'expeditor'],
                ],
                'children' => [],
            ]
        );
        $inputs = [
            new TransformInput(1, $resultsDef),
        ];
        $specs = [
            $tsAlerts
        ];
        $transformed = DataTransformer::transform($inputs, $specs);

        $output = DataTransformer::hydrateOutput(TicketResponseItem::class, $transformed, true, true);
        $this->assertIsArray($output);
        $this->assertCount(7, $output);
        $this->assertEquals(123, $output[0]->ticketId);
    }

    public function testHydrateOutputArrayOfObjects() {
        $inputDataAsArray = [];
        foreach ($this->inputData as $inputDatum) {
            $inputDataAsArray[] = $inputDatum->toArray();
        }
        $resultsDef = $inputDataAsArray;
        $tsAlerts = new TransformSpec(
            new StructurePath('#'), new StructurePath('#'), 1, [
                'name' => 'items',
                'isRoot' => true,
                'criteria' => null,
                'fields' => [
                    [ 'src' => 'ticketId'],
                    [ 'src' => 'mailId'],
                    [ 'src' => 'expeditor'],
                ],
                'children' => [],
            ]
        );
        $inputs = [
            new TransformInput(1, $resultsDef),
        ];
        $specs = [
            $tsAlerts
        ];
        $transformed = DataTransformer::transform($inputs, $specs);

        /** @var DummyEntity[] $output */
        $output = DataTransformer::hydrateOutput(null, $transformed, false, false);
        $this->assertIsArray($output);
        $this->assertCount(7, $output);
        $this->assertEquals(123, $output[0]['ticketId']);
    }


}