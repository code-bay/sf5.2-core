<?php

namespace CodeBay\Core\Tests\Unit\Core;

use League\Pipeline\Pipeline;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ClientMessageStage {

    public function processStage(Request $request, Response $response, $context)
    {
        if ($request instanceof Request) {
            return ['message' => $context];
        }
        return [];
    }
}

class ValidationStage {

    public function processStage(Request $request, Response $response, $context)
    {
        if (!is_array($context)) {
            return ['violations' => [
                'PAYLOAD_INCORRECT'
            ]];
        }
        return ['finalMessage' => sprintf('FINAL : %s', $context['message'])];
    }
}


class PipelineTest extends TestCase
{
    public function testPipeline() {

        $request = new Request();
        $response = new Response();

        $messageStage = new ClientMessageStage();
        $validationStage = new ValidationStage();

        (new Pipeline())
            ->pipe(
                function($payload) use ($request, $response, $messageStage) {
                    return $messageStage->processStage($request, $response, $payload);
                })
            ->pipe(
                function($payload) use ($request, $response, $validationStage) {
                    return $validationStage->processStage($request, $response, $payload);
            });

        $this->assertTrue(true);

    }
}