<?php

namespace CodeBay\Core\Tests\Unit\Core\JsonSchema;

use CodeBay\Core\JsonSchema\JsonSchemaMapper;
use Opis\JsonSchema\Validator;
use PHPUnit\Framework\TestCase;

class JsonSchemaMapperOasV2PatternPropertiesTest extends TestCase
{
    /**
     * @var JsonSchemaMapper
     */
    private $jsonMapper;

    public function setUp()
    {
        $this->jsonMapper = new JsonSchemaMapper();
    }

    public function testPatternProperties() {
        $validator = new Validator();
        $schema = \Opis\JsonSchema\Schema::fromJsonString('{"type":"object", "patternProperties": {"^x-": {"type": "object", "required": ["first"], "properties": {"first": {"type": "string"}}}} }');
        $data = new \StdClass();
        $varName = 'x-cb-uuid';
        $uuidObject = new \StdClass();
        $uuidObject->first = 'test';
        $data->$varName = $uuidObject;
        $result = $validator->schemaValidation($data, $schema);
        $this->assertFalse($result->hasErrors());
    }
}

