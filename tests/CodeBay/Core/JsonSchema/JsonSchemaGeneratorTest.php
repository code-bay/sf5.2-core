<?php

namespace CodeBay\Core\Tests\Unit\Core\JsonSchema;

use CodeBay\Core\JsonSchema\JsonSchemaGenerator;
use CodeBay\Core\JsonSchema\PropertyType;
use PHPUnit\Framework\TestCase;

/**
 * Class LittleDog
 * @jsonSchema --{"type": "object", "properties": {"name": {"type": "string"}}}
 * @package CodeBay\Core\Tests\Unit\Core\JsonSchema
 */
class LittleDog {
    /**
     * @var string
     */
    public $name;
}

class JsonSchemaGeneratorTest extends TestCase
{
    /**
     * @var JsonSchemaGenerator
     */
    private $jsonSchemaGenerator;

    public function setUp()
    {
        $this->jsonSchemaGenerator = new JsonSchemaGenerator();
    }

    public function testGenerateString()
    {
        $typeStr = 'string';
        $propertyType = new PropertyType($typeStr);
        $generated = $this->jsonSchemaGenerator->generateJsonSchema($propertyType);
        $this->assertEquals('{"type":"string"}', $generated);
    }

    public function testGenerateStringArray()
    {
        $typeStr = 'string[]';
        $propertyType = new PropertyType($typeStr);
        $generated = $this->jsonSchemaGenerator->generateJsonSchema($propertyType);
        $this->assertEquals('{"type":"array","items":{"type":"string"}}', $generated);
    }

    public function testGenerateObject()
    {
        $typeStr = '\\CodeBay\\Core\\Tests\\Unit\\Core\\JsonSchema\\LittleDog';
        $propertyType = new PropertyType($typeStr);
        $generated = $this->jsonSchemaGenerator->generateJsonSchema($propertyType);
        $this->assertEquals('{"type":"object","properties":{"name":{"type":"string"}}}', $generated);
    }
}

