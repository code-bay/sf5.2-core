<?php

namespace CodeBay\Core\Tests\Unit\Core\JsonSchema;

use CodeBay\Core\JsonSchema\JsonSchemaMapper;
use PHPUnit\Framework\TestCase;

/**
 * Class Dog
 * @jsonSchema --{"type": "object", "required": ["dogName"], "properties": {"dogName": {"type": "string"}}}
 * @package CodeBay\Core\Tests\Unit\Core\JsonSchema
 */
class Dog {
    /**
     * @var string
     */
    public $dogName;
}

/**
 * Class Cat
 * @jsonSchema --{"type": "object", "required": ["catName"], "properties": {"catName": {"type": "string"}}}
 * @package CodeBay\Core\Tests\Unit\Core\JsonSchema
 */
class Cat {
    /**
     * @var string
     */
    public $catName;
}

/**
 * Class Master
 * @jsonSchema --{"type": "object", "required": ["pet"], "properties": {"pet": {"$ref": "#/definitions/Cat"}}, "definitions": {"Cat": {"type": "object", "required": ["catName"], "properties": {"catName": {"type": "string"}}}}}
 * @package CodeBay\Core\Tests\Unit\Core\JsonSchema
 */
class Master {
    /**
     * @var \CodeBay\Core\Tests\Unit\Core\JsonSchema\Cat
     */
    protected $pet;

    /**
     * @return Cat
     */
    public function getPet(): Cat
    {
        return $this->pet;
    }

    /**
     * @param Cat $pet
     */
    public function setPet(Cat $pet)
    {
        $this->pet = $pet;
    }
}

/**
 * Class Forest
 * @package CodeBay\Core\Tests\Unit\Core\JsonSchema
 */
class Forest {
    /**
     * @var \CodeBay\Core\Tests\Unit\Core\JsonSchema\Dog[]|\CodeBay\Core\Tests\Unit\Core\JsonSchema\Cat[]
     */
    public $animals;
}

class JsonSchemaMapperForestTest extends TestCase
{
    /**
     * @var JsonSchemaMapper
     */
    private $jsonMapper;

    public function setUp()
    {
        $this->jsonMapper = new JsonSchemaMapper();
    }

    public function testComplexObject() {
        $jsonSchema = '{
            "type": "object",
            "properties": {
                "animals": {
                    "type": "array",
                    "items": {
                        "anyOf": [
                            {
                                "$ref": "#/definitions/Dog"
                            },
                            {
                                "$ref": "#/definitions/Cat"
                            }
                        ]
                    }
                }
            },
            "definitions": {
                "Dog": {
                    "type": "object",
                    "required": ["dogName"],
                    "properties": {
                        "dogName": {
                            "type": "string"
                        }
                    }
                },
                "Cat": {
                    "type": "object",
                    "required": ["catName"],
                    "properties": {
                        "catName": {
                            "type": "string"
                        }
                    }
                }
            }
        }';

        $jsonPayload = '{
            "animals": [
                {
                    "dogName": "medor"
                },
                {
                    "catName": "gaia"
                }
            ]
        }';
        $dryObject = new Forest();
        $mapped = $this->jsonMapper->mapObject($dryObject, $jsonSchema, $jsonPayload);
        $this->assertTrue($mapped instanceof Forest);
        $this->assertTrue($mapped->animals[0] instanceof Dog);
        $this->assertTrue($mapped->animals[1] instanceof Cat);
    }

    public function testMapNestedObjectWithRef() {
        $jsonSchema = '{
            "$ref": "#/definitions/Master",
            "definitions": {
                "Cat": {
                    "type": "object",
                    "required": ["catName"],
                    "properties": {
                        "catName": {
                            "type": "string"
                        }
                    }
                },
                "Master": {
                    "type": "object",
                    "required": ["pet"],
                    "properties": {
                        "pet": {
                            "$ref": "#/definitions/Cat"
                        }
                    }
                }
            }
        }';

        $jsonPayload = '{
            "pet": {
                "catName": "gaia"
            }
        }';
        $dryObject = new Master();
        $mapped = $this->jsonMapper->mapObject($dryObject, $jsonSchema, $jsonPayload);
        $this->assertTrue($mapped instanceof Master);
        $this->assertTrue($mapped->getPet() instanceof Cat);
        $this->assertEquals('gaia', $mapped->getPet()->catName);
    }

}

