<?php

namespace CodeBay\Core\Tests\Unit\Core\JsonSchema;

use CodeBay\Core\JsonSchema\PropertyType;
use PHPUnit\Framework\TestCase;

class PropertyTypeTest extends TestCase
{
    public function testGetType() {
        $typeStr = 'string';
        $propertyType = new PropertyType($typeStr);
        $this->assertEquals('string', $propertyType->getType());
        $this->assertEquals('string', $propertyType->getRawType());

        $typeStr = 'string[]';
        $propertyType = new PropertyType($typeStr);
        $this->assertEquals('string', $propertyType->getType());
        $this->assertEquals('string[]', $propertyType->getRawType());
    }

    public function testStringProperty()
    {
        $typeStr = 'string';
        $propertyType = new PropertyType($typeStr);
        $this->assertTrue($propertyType->isScalar());
        $this->assertTrue($propertyType->isString());
    }

    public function testStringArrayProperty()
    {
        $typeStr = 'string[]';
        $propertyType = new PropertyType($typeStr);
        $this->assertTrue($propertyType->isScalar());
        $this->assertTrue($propertyType->isString());
        $this->assertTrue($propertyType->isArray());
    }

    public function testComplexTypeProperty() {
        $typeStr = '\\Acme\\Service\\Dummy';
        $propertyType = new PropertyType($typeStr);
        $this->assertTrue($propertyType->isComplexType());
    }

    public function testArrayOfComplexTypeProperty() {
        $typeStr = '\\Acme\\Service\\Dummy[]';
        $propertyType = new PropertyType($typeStr);
        $this->assertTrue($propertyType->isComplexType());
        $this->assertTrue($propertyType->isArray());
        $this->assertTrue($propertyType->isArrayOfComplexType());
    }

    public function testMultipleTypeProperty() {
        $typeStr = '\\Acme\\Service\\Dummy|string[]|bool';
        $propertyType = new PropertyType($typeStr);
        $this->assertTrue($propertyType->isMultiple());
    }

    public function testGetPropertyTypeCandidates() {
        $typeStr = '\\Acme\\Service\\Dummy|string[]|bool';
        $propertyType = new PropertyType($typeStr);
        $this->assertTrue($propertyType->isMultiple());
        $candidates = $propertyType->getPropertyTypeCandidates();

        $currentCandidate = $candidates[0];
        $this->assertTrue($currentCandidate->isComplexType());
        $this->assertFalse($currentCandidate->isMultiple());
        $this->assertEquals('\\Acme\\Service\\Dummy', $currentCandidate->getType());

        $currentCandidate = $candidates[1];
        $this->assertTrue($currentCandidate->isString());
        $this->assertTrue($currentCandidate->isArray());
        $this->assertEquals('string', $currentCandidate->getType());

        $currentCandidate = $candidates[2];
        $this->assertTrue($currentCandidate->isBool());
        $this->assertFalse($currentCandidate->isArray());
        $this->assertEquals('bool', $currentCandidate->getType());
    }

    public function testSetAsNotArray() {
        $typeStr = '\\Acme\\Service\\Dummy[]|string[]|bool';
        $propertyType = new PropertyType($typeStr);
        $propertyType->setAsNotArray();
        $this->assertEquals('\\Acme\\Service\\Dummy|string|bool', $propertyType->getRawType());

        $typeStr = 'string[]';
        $propertyType = new PropertyType($typeStr);
        $propertyType->setAsNotArray();
        $this->assertEquals('string', $propertyType->getRawType());
        $this->assertFalse($propertyType->isArray());
        $this->assertTrue($propertyType->isString());

        $typeStr = 'string';
        $propertyType = new PropertyType($typeStr);
        $propertyType->setAsNotArray();
        $this->assertFalse($propertyType->isArray());
        $this->assertTrue($propertyType->isString());
    }
}

