<?php

namespace CodeBay\Core\Tests\Unit\Core\JsonSchema;

use CodeBay\Core\JsonMappable;
use CodeBay\Core\JsonSchema\JsonSchemaMapper;
use PHPUnit\Framework\TestCase;

/**
 * Class NameObject
 * @jsonSchema --{"type":"object","properties":{"name": {"type": "string"}}}
 * @package CodeBay\Core\Tests\Unit\Core\JsonSchema
 */
class NameObject {
    /**
     * @var string
     */
    public $name;
}

/**
 * Class Project
 * @jsonSchema --{"type": "object", "properties": {"task": {"$ref": "#/definitions/Name"}}, "definitions": "Name": {"type":"object","properties":{"name": {"type": "string"}}}}
 * @package CodeBay\Core\Tests\Unit\Core\JsonSchema
 */
class Project {
    /**
     * @var \CodeBay\Core\Tests\Unit\Core\JsonSchema\NameObject
     */
    public $task;
}

/**
 * Class TasksProject
 * @jsonSchema --{"type": "object", "properties": {"tasks": {"type": "array", "items": {"$ref": "#/definitions/Name"}}}, "definitions": "Name": {"type":"object","properties":{"name": {"type": "string"}}}}
 * @package CodeBay\Core\Tests\Unit\Core\JsonSchema
 */
class TasksProject {
    /**
     * @var \CodeBay\Core\Tests\Unit\Core\JsonSchema\NameObject[]
     */
    public $tasks;
}

/**
 * Class ComplexObject
 * @package CodeBay\Core\Tests\Unit\Core\JsonSchema
 */
class ComplexObject {

    /**
     * @var string
     */
    public $name;
    /**
     * @var \CodeBay\Core\Tests\Unit\Core\JsonSchema\Project[]
     */
    public $projects;
    /**
     * @var string[]|\CodeBay\Core\Tests\Unit\Core\JsonSchema\NameObject
     */
    public $tasks;
    /**
     * @var string[]|\CodeBay\Core\Tests\Unit\Core\JsonSchema\NameObject[]
     */
    public $names;

}

/**
 * @jsonSchema --{"type":"object","properties":{"name":{"type":"string"},"schema":{"type":"object"}}}
 */
class SampleDefinition
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var
     */
    public $schema;
}

/**
 * @jsonSchema --{"type":"object","properties":{"projectId":{"type":"string"},"apiId":{"type":"string"},"definitionName":{"type":"string"},"payload":{"type":"object","properties":{"name":{"type":"string"}}}},"definitions":[]}
 */
class UpdateDefinitionNameMessage extends JsonMappable
{
    /**
     * @var
     */
    public $projectId;

    /**
     * @var
     */
    public $apiId;

    /**
     * @var
     */
    public $definitionName;

    /**
     * @var
     */
    public $payload;

    public function getProjectId() { return $this->projectId; }
    public function getPayload() { return $this->payload; }
}


class JsonSchemaMapperTest extends TestCase
{
    /**
     * @var JsonSchemaMapper
     */
    private $jsonMapper;

    public function setUp()
    {
        $this->jsonMapper = new JsonSchemaMapper();
    }

    public function testSimpleObject() {
        $payload = '{
            "name": "Dot"
        }';
        $jsonSchema = '{
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                }
            }
        }';
        $nameObject = new NameObject();
        $nameObject = $this->jsonMapper->mapObject($nameObject, $jsonSchema, $payload);

        $this->assertTrue($nameObject instanceof NameObject);
        $this->assertEquals('Dot', $nameObject->name);
    }

    public function testRawType() {
        $jsonPayload = 'Dot';
        $jsonSchema = '{
            "type": "string"
        }';
        $object = null;
        $mapped = $this->jsonMapper->mapObject($object, $jsonSchema, $jsonPayload);
        $this->assertEquals('Dot', $mapped);
    }

    public function testArrayType() {
        $jsonPayload = '[ "item1", "item2" ]';
        $jsonSchema = '{
            "type": "array",
            "items": {
                "type": "string"
            }
        }';

        $object = null;
        $mapped = $this->jsonMapper->mapObject($object, $jsonSchema, $jsonPayload);
        $this->assertCount(2, $mapped);
        $this->assertEquals('item1', $mapped[0]);
        $this->assertEquals('item2', $mapped[1]);
    }

    public function testRef() {
        $jsonSchema = '{
            "$ref": "#/definitions/Project",
            "definitions": {
                "Project": {
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string"
                        }
                    }
                }
            }
        }';
        $jsonPayload = '{
            "name": "mess"
        }';

        $object = new NameObject();
        $mapped = $this->jsonMapper->mapObject($object, $jsonSchema, $jsonPayload);
        $this->assertTrue($mapped instanceof NameObject);
        $this->assertEquals('mess', $mapped->name);
    }

    public function testNestedObject() {
        $jsonSchema = '{
            "type": "object",
            "properties": {
                "task": {
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string"
                        }
                    }
                }
            }
        }';
        $jsonPayload = '{
            "task": {
                "name": "main"
            }
        }';
        $project = new Project();
        $mapped = $this->jsonMapper->mapObject($project, $jsonSchema, $jsonPayload);

        $this->assertTrue($mapped instanceof Project);
        $this->assertTrue($mapped->task instanceof NameObject);
        $this->assertEquals('main', $mapped->task->name);
    }

    public function testObjectPropsAsArrayOfObjects() {
        $jsonSchema = '{
            "type": "object",
            "properties": {
                "tasks": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "name": {
                                "type": "string"
                            }
                        }
                    }
                }
            }
        }';
        $jsonPayload = '{
            "tasks": [
                { "name": "main" },   
                { "name": "second" }
            ]
        }';
        $project = new TasksProject();
        $mapped = $this->jsonMapper->mapObject($project, $jsonSchema, $jsonPayload);

        $this->assertTrue($mapped instanceof TasksProject);
        $this->assertCount(2, $mapped->tasks);
        $this->assertTrue($mapped->tasks[0] instanceof NameObject);
        $this->assertEquals('main', $mapped->tasks[0]->name);
    }

    public function testArrayOfObjects() {
        $jsonSchema = '{
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "name": {
                        "type": "string"
                    }
                }
            }
        }';
        $jsonPayload = '[
            { "name": "main" },
            { "name": "second" }
        ]';
        $dryObject = new NameObject();
        $mapped = $this->jsonMapper->mapObject($dryObject, $jsonSchema, $jsonPayload);

        $this->assertCount(2, $mapped);
        $this->assertTrue($mapped[0] instanceof NameObject);
        $this->assertEquals('main', $mapped[0]->name);
        $this->assertEquals('second', $mapped[1]->name);
    }

    public function testArrayOfRefs() {
        $jsonSchema = '{
            "type": "array",
            "items": {
                "$ref": "#/definitions/Project"
            },
            "definitions": {
                "Project": {
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string"
                        }
                    }
                }
            }
        }';
        $jsonPayload = '[
            { "name": "main" },
            { "name": "second" }
        ]';
        $dryObject = new NameObject();
        $mapped = $this->jsonMapper->mapObject($dryObject, $jsonSchema, $jsonPayload);

        $this->assertCount(2, $mapped);
        $this->assertTrue($mapped[0] instanceof NameObject);
        $this->assertEquals('main', $mapped[0]->name);
        $this->assertEquals('second', $mapped[1]->name);
    }

    public function testOneOf() {
        $jsonSchema = '{
            "oneOf": [
                {
                    "type": "string"
                },
                {
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string"
                        }
                    }
                }
            ]
        }';
        $jsonPayload = 'simpleString';
        $dryObject = null;
        $mapped = $this->jsonMapper->mapObject($dryObject, $jsonSchema, $jsonPayload);
        $this->assertEquals('simpleString', $mapped);

        $jsonPayload = '{
            "name": "myProject"
        }';
        $dryObject = new NameObject();
        $mapped = $this->jsonMapper->mapObject($dryObject, $jsonSchema, $jsonPayload);
        $this->assertTrue($mapped instanceof NameObject);
        $this->assertEquals('myProject', $mapped->name);
    }

    public function testAnyOf() {
        $jsonSchema = '{
            "anyOf": [
                {
                    "type": "string"
                },
                {
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string"
                        }
                    }
                }
            ]
        }';
        $jsonPayload = 'simpleString';
        $dryObject = null;
        $mapped = $this->jsonMapper->mapObject($dryObject, $jsonSchema, $jsonPayload);
        $this->assertEquals('simpleString', $mapped);

        $jsonPayload = '{
            "name": "myProject"
        }';
        $dryObject = new NameObject();
        $mapped = $this->jsonMapper->mapObject($dryObject, $jsonSchema, $jsonPayload);
        $this->assertTrue($mapped instanceof NameObject);
        $this->assertEquals('myProject', $mapped->name);
    }

    public function testComplexObject() {
        $jsonSchema = '{
            "type": "object",
            "properties": {
                "projects": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "task": {
                                "type": "array",
                                "items": {
                                    "type": "object",
                                    "properties": {
                                        "name": {
                                            "type": "string"
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "name": {
                    "type": "string"
                },
                "tasks": {
                    "anyOf": [
                        {
                            "type": "array",
                            "items": {
                                "type": "string"
                            }
                        },
                        {
                            "$ref": "#/definitions/Project"
                        }
                    ]
                },
                "names": {
                    "anyOf": [
                        {
                            "type": "array",
                            "items": {
                                "type": "string"
                            }
                        },
                        {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/Project"
                            }
                        }
                    ]
                }
            },
            "definitions": {
                "Project": {
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string"
                        }
                    }
                }
            }
        }';

        $jsonPayload = '{
            "name": "hello",
            "projects": [
                {
                    "task": [
                        {"name": "task1"},
                        {"name": "task2"}
                    ]
                },
                {
                    "task": [
                        {"name": "task3"}
                    ]
                }
            ],
            "tasks": {
                "name": "todo"
            },
            "names": [
                {
                    "name": "project1"
                },
                {
                    "name": "project2"
                }
            ]
        }';
        $dryObject = new ComplexObject();
        $mapped = $this->jsonMapper->mapObject($dryObject, $jsonSchema, $jsonPayload);
        $this->assertTrue($mapped instanceof ComplexObject);
        $this->assertEquals('hello', $mapped->name);
        $this->assertTrue($mapped->tasks instanceof NameObject);
        $this->assertEquals('todo', $mapped->tasks->name);
    }

    public function testTypeObject() {

        $jsonSchema = '{
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "schema": {
                    "type": "object"
                }
            }
        }';
        $jsonPayload = '{
            "name": "WidgetDetail",
            "schema": {
                "type": "object"
            }
        }';
        $dryObject = new SampleDefinition();
        $mapped = $this->jsonMapper->mapObject($dryObject, $jsonSchema, $jsonPayload);

        $this->assertTrue($mapped instanceof SampleDefinition);
        $this->assertEquals('WidgetDetail', $mapped->name);
        $this->assertEquals('object', $mapped->schema['type']);

    }

    public function testExtractPropertyType() {
        $dryObject = new SampleDefinition();
        $extracted = $this->jsonMapper->extractPropertyType($dryObject, 'schema');
        $this->assertTrue($extracted->isMixed());
    }


    public function testRealLifeError() {
        $payload = '{
            "projectId": "1196",
            "payload": {
                "name": "myName"
            }
        }';
        $jsonSchema = '{
            "type": "object",
            "properties": {
                "projectId": {
                    "type": "string"
                },
                "payload": {
                    "type": "object",
                    "properties": {
                        "name": {
                            "type": "string"
                         }
                    }
                }
            }
        }';
        $nameObject = new UpdateDefinitionNameMessage();
        $nameObject = $this->jsonMapper->mapObject($nameObject, $jsonSchema, $payload);

        $this->assertTrue($nameObject instanceof UpdateDefinitionNameMessage);
        $this->assertEquals('1196', $nameObject->getProjectId());
        $this->assertEquals('myName', $nameObject->getPayload()->name);
    }

}

