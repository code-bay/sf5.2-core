<?php

namespace CodeBay\Core\Repository;

interface AbstractQueryInterface
{
    /**
     * @return string
     */
    public function getQuery();

    /**
     * @return array
     */
    public function getBinding();

    /**
     * @return string|null
     */
    public function getResultClass();
}