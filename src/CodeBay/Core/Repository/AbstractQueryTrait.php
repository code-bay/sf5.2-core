<?php

namespace CodeBay\Core\Repository;

use Doctrine\ORM\EntityManagerInterface;

trait AbstractQueryTrait
{
    /**
     * @param $queryObject
     * @param bool $singleResult
     * @return array|null|object
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    protected function runQuery(
        AbstractQueryInterface $queryObject, $singleResult = false
    ) {
        //Get connection
        /** @var EntityManagerInterface $em */
        $em = $this->getEntityManager();
        $conn = $em->getConnection();

        //Prepare and bind query
        $sql = $queryObject->getQuery();
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery($queryObject->getBinding());

        //Actually run the query
        $rawResults = $stmt->fetchAll();

        //Parse results
        $parsedResults = null;
        $resultClass = $queryObject->getResultClass();
        if ($resultClass) {
            if ($singleResult) {
                if ($rawResults) {
                    return new $resultClass($rawResults[0]);
                }
            } else {
                $parsedResults = [];
                foreach ($rawResults as $rawResult) {
                    $parsedResults[] = new $resultClass($rawResult);
                }
            }
        }

        return $parsedResults;
    }
}