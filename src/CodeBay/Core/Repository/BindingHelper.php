<?php


namespace CodeBay\Core\Repository;


class BindingHelper
{
    /**
     * @param $values
     * @param string $prefix
     * @return array
     */
    public static function getBindingValues($values, $prefix = 'v') {
        if (!is_array($values) || !$prefix) {
            return [];
        }
        $bindingValues = [];
        foreach ($values as $i => $value) {
            $currentKey = sprintf('%s_%d', $prefix, $i);
            $bindingValues[$currentKey] = $value;
        }
        return $bindingValues;
    }

    /**
     * @param $values
     * @param string $prefix
     * @return array
     */
    public static function getBindingKeys($values, $prefix = 'v') {
        if (!is_array($values) || !$prefix) {
            return [];
        }
        $bindingKeys = [];
        foreach ($values as $i => $value) {
            $bindingKeys[] = sprintf(':%s_%d', $prefix, $i);
        }
        return $bindingKeys;
    }

    /**
     * @param $values
     * @param string $prefix
     * @return string|null
     */
    public static function getInBindingString($values, $prefix = 'v') {
        $bindingKeys = self::getBindingKeys($values, $prefix);
        if (count($bindingKeys) === 0) {
            return null;
        }
        return implode(',', $bindingKeys);
    }

}