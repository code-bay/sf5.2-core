<?php

namespace CodeBay\Core\Pipeline;

use Symfony\Component\HttpFoundation\Request;

interface ApiPipelineInterface
{
    /**
     * @param Request $request
     * @param $context
     */
    public function run(Request $request, ExceptionAwareContext $context);
}