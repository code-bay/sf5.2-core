<?php

namespace CodeBay\Core\Pipeline;

use Symfony\Component\HttpFoundation\Request;

interface ApiActionStageInterface
{
    public function processStage(Request $request, ExceptionAwareContext $context);
}