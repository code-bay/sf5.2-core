<?php

namespace CodeBay\Core\Pipeline;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiPipeline implements ApiPipelineInterface
{
    /**
     * @var ApiActionStageInterface[]
     */
    protected $stages;

    /**
     * @var ApiActionStageInterface
     */
    protected $exceptionPipeline;

    /**
     * AbstractApiPipeline constructor.
     * @param ApiActionStageInterface[] $stages
     * @param ApiActionStageInterface $exceptionPipeline
     */
    public function __construct(array $stages, ApiActionStageInterface $exceptionPipeline)
    {
        $this->stages = $stages;
        $this->exceptionPipeline = $exceptionPipeline;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $context
     */
    public function run(Request $request, ExceptionAwareContext $context) {
        try {
            foreach ($this->stages as $stage) {
                $stage->processStage($request, $context);
            }
        } catch (\Throwable $e) {
            $context->setException($e);
            $this->exceptionPipeline->processStage($request, $context);
        }
    }
}