<?php

namespace CodeBay\Core\Pipeline;

use Symfony\Component\HttpFoundation\Response;

interface ExceptionAwareContext
{
    /**
     * @return \Exception
     */
    public function getException();

    /**
     * @param \Exception $e
     * @return mixed
     */
    public function setException($e);

    /**
     * @return Response
     */
    public function getResponse();

    /**
     * @param Response $response
     * @return mixed
     */
    public function setResponse($response);

    /**
     * @return string
     */
    public function getTwigTemplate();

    /**
     * @param $template
     * @return mixed
     */
    public function setTwigTemplate($template);

}