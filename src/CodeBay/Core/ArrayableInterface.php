<?php

namespace CodeBay\Core;

interface ArrayableInterface
{
	/**
	 * @return array
	 */
	public function toArray();
}