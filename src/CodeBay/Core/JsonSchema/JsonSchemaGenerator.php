<?php

namespace CodeBay\Core\JsonSchema;

use DocBlockReader\Reader;

class JsonSchemaGenerator
{
    /**
     * @param PropertyType $propertyType
     * @return string|string[]
     */
    public function generateJsonSchema(PropertyType $propertyType) {
        $schema = null;
        $isArray = $propertyType->isArray();
        $isArrayOfScalar = $isArray && $propertyType->isScalar();
        $isScalar = !$isArray && $propertyType->isScalar();
        $isComplexType = !$isArray && $propertyType->isComplexType();
        $isArrayOfComplexType = $propertyType->isArrayOfComplexType();

        if ($isArrayOfScalar) {
            $schema = $this->generatedScalarArray($propertyType);
        } else if ($isScalar) {
            $schema = $this->generatedScalar($propertyType);
        } else if ($isComplexType) {
            $schema = $this->generatedComplexType($propertyType);
        } else if ($isArrayOfComplexType) {
            $schema = $this->generatedComplexTypeArray($propertyType);
        }

        return $this->formatSchema($schema);
    }

    /**
     * @param PropertyType $propertyType
     * @return string
     */
    public function generatedScalar(PropertyType $propertyType) {
        if ($propertyType->isString()) {
            return '{"type": "string"}';
        }
    }

    /**
     * @param PropertyType $propertyType
     * @return false|string
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function generatedComplexType(PropertyType $propertyType) {
        $className = $propertyType->getType();
        $instance = new $className();
        $reflectionClass = new \ReflectionClass($instance);
        $reflectionClass->getDocComment();
        $reader = new Reader($className);
        $jsonSchema = $reader->getParameter('jsonSchema');
        return substr($jsonSchema, 2);
    }

    /**
     * @param PropertyType $propertyType
     * @return string
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function generatedComplexTypeArray(PropertyType $propertyType) {
        $className = $propertyType->getType();
        $instance = new $className();
        $reflectionClass = new \ReflectionClass($instance);
        $reflectionClass->getDocComment();
        $reader = new Reader($className);
        $jsonSchema = $reader->getParameter('jsonSchema');
        return sprintf('{"type": "array", "items":%s}', substr($jsonSchema, 2));
    }

    /**
     * @param PropertyType $propertyType
     * @return string
     */
    public function generatedScalarArray(PropertyType $propertyType) {
        if ($propertyType->isString()) {
            return '{"type": "array", "items": {"type": "string"}}';
        }
    }

    /**
     * @param $schema
     * @return string|string[]
     */
    protected function formatSchema($schema) {
        return str_replace(' ', '', $schema);
    }
}