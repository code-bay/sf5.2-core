<?php

namespace CodeBay\Core\JsonSchema;

class PropertyType
{
    protected $rawType;

    /**
     * JsonSchemaType constructor.
     * @param $rawType
     * @throws \Exception
     */
    public function __construct($rawType)
    {
        if (!isset($rawType)) {
            throw new \Exception('Type raw value cannot be null');
        }
        $this->rawType = $rawType;
    }

    /**
     * @return bool
     */
    public function isArray() {
        return strpos($this->rawType, '[]') !== false;
    }

    /**
     * @return bool
     */
    public function isComplexType() {
        return !($this->isScalar() || $this->isUndefinedArray() || $this->isMixed());
    }

    /**
     * @return bool
     */
    public function isUndefinedArray() {
        return $this->getType() === 'array';
    }

    /**
     * @return bool
     */
    public function isMixed() {
        return $this->getType() === 'mixed';
    }

    /**
     * @return bool
     */
    public function isString() {
        return $this->getType() === 'string';
    }

    /**
     * @return bool
     */
    public function isBool() {
        return $this->getType() === 'bool' || $this->getType() === 'boolean';
    }

    /***
     * @return bool
     */
    public function isInteger() {
        return $this->getType() === 'int' || $this->getType() === 'integer';
    }

    /**
     * @return bool
     */
    public function isFloat() {
        return $this->getType() === 'float';
    }

    /**
     * @return bool
     */
    public function isDouble() {
        return $this->getType() === 'double';
    }

    /**
     * @return bool
     */
    public function isScalar() {
        return $this->isBool() || $this->isInteger() || $this->isFloat() || $this->isDouble() || $this->isString();
    }

    /**
     * @return string|string[]
     */
    public function getType() {
        return str_replace('[]', '', $this->rawType);
    }

    /**
     * @return mixed
     */
    public function getRawType() {
        return $this->rawType;
    }

    /**
     * @return bool
     */
    public function isArrayOfComplexType() {
        return $this->isArray() && $this->isComplexType();
    }

    /**
     * @return bool
     */
    public function isMultiple() {
        return count(explode('|', $this->rawType)) > 1;
    }

    /**
     * @return array|PropertyType[]
     * @throws \Exception
     */
    public function getPropertyTypeCandidates() {
        $candidates = [];
        if ($this->isMultiple()) {
            $exploded = explode('|', $this->rawType);
            foreach ($exploded as $item) {
                $candidates[] = new PropertyType($item);
            }
        }
        return $candidates;
    }

    /**
     * @return PropertyType
     */
    public function setAsNotArray() {
        $this->rawType = str_replace('[]', '', $this->rawType);
        return $this;
    }

}