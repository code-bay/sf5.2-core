<?php

namespace CodeBay\Core\JsonSchema;

use CodeBay\Core\Tools\JsonSerializer;
use DocBlockReader\Reader;
use Opis\JsonSchema\Validator;

class JsonSchemaMapper
{
    const ERR_HYDRATE_ONE_OF_ANY_OF = 1000;
    const ERR_NO_MATCHING_ONE_OF_ANY_OF = 1001;
    const ERR_UNHANDLED_JSON_SCHEMA = 1002;

    /**
     * @var JsonSchemaGenerator
     */
    private $jsonSchemaGenerator;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * JsonSchemaMapper constructor.
     */
    public function __construct()
    {
        $this->validator = new Validator();
    }

    /**
     * @param $dryInstance
     * @param $jsonSchema
     * @param $jsonPayload
     * @param null $package
     * @param null $definitionNamespace
     * @return mixed
     * @throws \ReflectionException
     */
    public function mapObject($dryInstance, $jsonSchema, $jsonPayload, $package = null, $definitionNamespace = null) {
        $schema = $this->unSerializeJson($jsonSchema);
        $payload = $this->unSerializeJson($jsonPayload);
        if ($payload === null) return $jsonPayload;
        $definitions = key_exists('definitions', $schema) ? $schema['definitions'] : [];
        return $this->hydrateObject(
            $dryInstance, $schema, $payload, $definitions, null, $package, $definitionNamespace
        );
    }

    /**
     * @param $object
     * @param $schema
     * @param $payload
     * @param array $definitions
     * @param null|PropertyType $currentPropertyType
     * @param null $package
     * @param null $definitionNamespace
     * @return mixed
     * @throws \ReflectionException
     */
    public function hydrateObject(
        $object, $schema, $payload, $definitions = [], $currentPropertyType = null, $package = null, $definitionNamespace = null
    ) {

        if ($payload === null) return null;

        $isObjectWithProperties = $this->isJsonSchemaObject($schema) && $this->jsonSchemaHasProperties($schema);
        $isObjectWithoutProperties = $this->isJsonSchemaObject($schema) && !$this->jsonSchemaHasProperties($schema);
        $isReference = $this->isJsonSchemaRef($schema);
        $isScalar = $this->isJsonSchemaScalar($schema);
        $isArray = $this->isJsonSchemaArray($schema);
        $isOneOf = $this->isJsonSchemaOneOf($schema);
        $isAllOf = $this->isJsonSchemaAllOf($schema);
        $isAnyOf = $this->isJsonSchemaAnyOf($schema);

        // Handle object with properties
        if ($isObjectWithProperties) {
            $properties = $schema['properties'];
            foreach ($properties as $propertyName => $property) {
                if (!key_exists($propertyName, $payload)) {
                    continue;
                }

                $propertyPayloadValue = $payload[$propertyName];
                $subJsonSchema = $schema['properties'][$propertyName];
                $propertyType = $this->extractPropertyType($object, $propertyName);
                $child = new \StdClass();
                try {
                    if ($propertyType && !$propertyType->isMultiple() && $propertyType->isComplexType()) {
                        $complexType = $propertyType->getType();
                        $child = $this->instantiateClass($complexType);
                    }
                    $newValueProperty = $this->hydrateObject($child, $subJsonSchema, $propertyPayloadValue, $definitions, $propertyType, $package, $definitionNamespace);
                    $this->setObjectProperty($object, $propertyName, $newValueProperty);
                } catch (\Exception $e) {
                    throw $e;
                }
            }

            // Handle pattern properties
            if (key_exists('patternProperties', $schema) && !($payload instanceof \StdClass)) {
                $patternProperties = $schema['patternProperties'];
                $payloadKeys = array_keys($payload);
                foreach ($patternProperties as $ppKey => $patternProperty) {
                    foreach ($payloadKeys as $payloadKey) {
                        if (preg_match("/$ppKey/", $payloadKey)) {
                            $object->$payloadKey = $payload[$payloadKey];
                        }
                    }
                }
            }

            return $object;

        // Handle reference
        } else if ($isReference) {

            $refSchema = $this->getJsonRef($definitions, $schema['$ref']);
            return $this->hydrateObject($object, $refSchema, $payload, $definitions, null, $package, $definitionNamespace);

        // Handle scalar
        } else if ($isScalar) {

            $this->checkScalarValue($schema['type'], $payload);
            return $payload;

        // Handle array
        } else if ($isArray) {

            $itemsSchema = $this->getJsonSchemaItemsSchema($schema);
            $itemObjectClass = null;
            if ($object !== null) {
                $itemObjectClass = get_class($object);
            }

            if ($currentPropertyType !== null
                && $currentPropertyType->isComplexType()
                && !$currentPropertyType->isMultiple()
            ) {
                $itemObjectClass = $currentPropertyType->getType();
            }

            $arrContainer = [];
            foreach ($payload as $item) {
                if ($itemObjectClass !== null) {
                    $newItemObject = $this->instantiateClass($itemObjectClass);
                } else {
                    $newItemObject = new \StdClass();
                }
                if ($currentPropertyType) {
                    $currentPropertyType->setAsNotArray();
                }
                $arrContainer[] = $this->hydrateObject($newItemObject, $itemsSchema, $item, $definitions, $currentPropertyType, $package, $definitionNamespace);
            }

            return $arrContainer;

        // Handle oneOf, anyOf
        } else if ($isOneOf || $isAnyOf) {

            if ($isOneOf) {
                $schemasToCheck = $schema['oneOf'];
            } else {
                $schemasToCheck = $schema['anyOf'];
            }

            $validatingPayloadSchema = null;
            foreach ($schemasToCheck as $currentSchema) {
                if ($validatingPayloadSchema !== null) continue;
                try {
                    $currentSchemaWithDefinitions = array_merge($currentSchema, ['definitions' => $definitions]);
                    if ($this->validatePayload(json_encode($currentSchemaWithDefinitions), $payload)) {
                        $validatingPayloadSchema = $currentSchemaWithDefinitions;
                    }
                } catch (\Exception $e) {
                    throw new \Exception(
                        "Failed to hydrate oneOf/anyOf schema part", self::ERR_HYDRATE_ONE_OF_ANY_OF, $e
                    );
                }
            }

            if ($validatingPayloadSchema === null) {
                throw new \Exception('No valid schema found', self::ERR_NO_MATCHING_ONE_OF_ANY_OF);
            }

            if ($currentPropertyType && $currentPropertyType->isMultiple()) {
                $validType = null;
                $validTypeSchema = null;
                $typeCandidates = $currentPropertyType->getPropertyTypeCandidates();
                foreach ($typeCandidates as $candidatePropertyType) {
                    if ($validType !== null) continue;
                    $generatedSchema = $this->getJsonSchemaGenerator()->generateJsonSchema($candidatePropertyType);
                    if ($this->validatePayload($generatedSchema, $payload)) {
                        $validType = $candidatePropertyType;
                        $validTypeSchema = $this->unSerializeJson($generatedSchema);
                    }
                }
                if ($validTypeSchema !== null) {
                    if ($validType->isComplexType()) {
                        $currentComplexType = $validType->getType();
                        $object = $this->instantiateClass($currentComplexType);
                    }
                    return $this->hydrateObject($object, $validTypeSchema, $payload, $definitions, $validType, $package, $definitionNamespace);
                }
            } else {
                if (!$currentPropertyType &&
                    key_exists('$ref', $validatingPayloadSchema) &&
                    $validatingPayloadSchema['$ref']
                ) {
                    if (!$definitionNamespace) {
                        throw new \Exception('Unhandled use case');
                    } else {
                        $currentComplexType = $validatingPayloadSchema['$ref'];
                        $currentComplexType = str_replace(
                            '#/definitions/', sprintf('\\%s\\', $definitionNamespace), $currentComplexType
                        );
                        $object = $this->instantiateClass($currentComplexType);
                    }
                }
                return $this->hydrateObject($object, $validatingPayloadSchema, $payload, $definitions, null, $package, $definitionNamespace);
            }

            // Erreur, aucun type n'est possible pour l'hydratation
            throw new \Exception('Cannot map any of the provided schemas');

        // Handle allOf
        } else if ($isAllOf) {
            $mergedSchema = [];
            foreach ($schema['allOf'] as $item) {
                $mergedSchema = array_merge($mergedSchema, $item);
            }
            return $this->hydrateObject($object, $mergedSchema, $payload, $definitions, null, $package, $definitionNamespace);

        // Handle object without propertoes
        } else if ($isObjectWithoutProperties) {

            return $payload;

        // Unhandled json schema
        } else {
            $jsonSchema = json_encode($schema);
            throw new \Exception("Json schema not handled [$jsonSchema]", self::ERR_UNHANDLED_JSON_SCHEMA);
        }
    }

    /**
     * @param $scalarType
     * @param $payload
     * @throws \Exception
     */
    public function checkScalarValue($scalarType, $payload) {
        if (is_array($payload)) {
            throw new \Exception(sprintf("%s does not match scalar type %s", json_encode($payload), $scalarType));
        }
    }

    /**
     * @param $json
     * @return mixed
     */
    public function unSerializeJson($json) {
        return JsonSerializer::decode($json);
    }

    /**
     * @param $object
     * @param $propertyName
     * @return PropertyType|null
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function extractPropertyType($object, $propertyName) {
        if ($object !== null) {
            $rc = new \ReflectionClass($object);
            if ($rc->hasProperty($propertyName)) {
                $className = get_class($object);
                $reader = new Reader($className, $propertyName, 'property');
                $propertyTypeString = $reader->getParameter('var');
                if ($propertyTypeString === true) {
                    return new PropertyType('mixed');
                }
                if (is_array($propertyTypeString)) {
                    return new PropertyType('array');
                }
                return new PropertyType($propertyTypeString);
            }
        }
        return null;
    }

    /**
     * @param $jsonSchema
     * @return bool
     */
    public function isJsonSchemaRef($jsonSchema) {
        return key_exists('$ref', $jsonSchema);
    }

    /**
     * @param $jsonSchema
     * @return bool
     */
    public function isJsonSchemaScalar($jsonSchema) {
        if (key_exists('type', $jsonSchema)) {
            return in_array($jsonSchema['type'], [
                'boolean', 'integer', 'number', 'string'
            ]);
        }
        return false;
    }

    /**
     * @param $jsonSchema
     * @return bool
     */
    public function isJsonSchemaArray($jsonSchema) {
        return key_exists('type', $jsonSchema) && $jsonSchema['type'] === 'array';
    }

    /**
     * @param $jsonSchema
     * @return bool
     */
    public function isJsonSchemaOneOf($jsonSchema) {
        return key_exists('oneOf', $jsonSchema);
    }

    /**
     * @param $jsonSchema
     * @return bool
     */
    public function isJsonSchemaAllOf($jsonSchema) {
        return key_exists('allOf', $jsonSchema);
    }

    /**
     * @param $jsonSchema
     * @return bool
     */
    public function isJsonSchemaAnyOf($jsonSchema) {
        return key_exists('anyOf', $jsonSchema);
    }

    /**
     * @param $jsonSchema
     * @return bool
     */
    public function getJsonSchemaItemsSchema($jsonSchema) {
        if ($this->isJsonSchemaArray($jsonSchema)) {
            return $jsonSchema['items'];
        }
        return null;
    }

    /**
     * @param $definitions
     * @param $ref
     * @return mixed|null
     */
    public function getJsonRef($definitions, $ref) {
        $ref = str_replace('#/definitions/', '', $ref);
        if (key_exists($ref, $definitions)) {
            return $definitions[$ref];
        }
        return null;
    }

    /**
     * @param $jsonSchema
     * @return bool
     */
    public function isJsonSchemaObject($jsonSchema) {
        $doesNotHaveTypeKey = !key_exists('type', $jsonSchema);
        return $doesNotHaveTypeKey || (!$doesNotHaveTypeKey && $jsonSchema['type'] === 'object');
    }

    /**
     * @param $jsonSchema
     * @return bool
     */
    public function jsonSchemaHasProperties($jsonSchema) {
        return key_exists('properties', $jsonSchema) && is_array($jsonSchema['properties']);
    }

    /**
     * @return JsonSchemaGenerator
     */
    public function getJsonSchemaGenerator(): JsonSchemaGenerator
    {
        if ($this->jsonSchemaGenerator === null) {
            $this->jsonSchemaGenerator = new JsonSchemaGenerator();
        }
        return $this->jsonSchemaGenerator;
    }

    /**
     * @param $jsonSchema
     * @param $payload
     * @return bool
     * @throws \Exception
     */
    public function validatePayload($jsonSchema, $payload) {
        $unserializedPayload = json_decode(json_encode($payload));
        $schema = \Opis\JsonSchema\Schema::fromJsonString($jsonSchema);
        try {
            $result = $this->validator->schemaValidation($unserializedPayload, $schema);
        } catch (\Exception $e) {
            throw new \Exception(sprintf('Failed to validate payload %s', json_encode($payload)));
        }
        return !$result->hasErrors();
    }

    /**
     * @param $fullClassPath
     * @return mixed
     */
    public function instantiateClass($fullClassPath) {
        return new $fullClassPath();
    }

    /**
     * @param $objectInstance
     * @param $propertyName
     * @param $value
     * @return mixed
     * @throws \ReflectionException
     */
    public function setObjectProperty($objectInstance, $propertyName, $value) {
        $rc = new \ReflectionClass($objectInstance);
        $propertyName = str_replace('$', '', $propertyName);

        if (
            $rc->hasProperty($propertyName) &&
            $rc->getProperty($propertyName)->isPublic() ||
            $objectInstance instanceof \StdClass
        ) {
            $objectInstance->$propertyName = $value;
        } else {
            $setterName = sprintf('set%s%s', strtoupper($propertyName[0]), substr($propertyName, 1));
            if ($rc->hasMethod($setterName)) {
                $objectInstance->$setterName($value);
            }
        }

        return $objectInstance;
    }

}