<?php

namespace CodeBay\Core;

class Arrayable implements ArrayableInterface
{
    /**
     * Arrayable constructor.
     * @param array|null $mixed
     * @throws \ReflectionException
     */
	public function __construct(array $mixed = null)
	{
		if ($mixed !== null) {
			$reflect = new \ReflectionClass($this);
			$props = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED);
			/** @var \ReflectionProperty $prop */
			foreach ($props as $prop) {
				$propName = $prop->getName();
				if (key_exists($prop->getName(), $mixed)) {
					$this->$propName = $mixed[$propName];
				}
			}
		}
	}

	/**
	 * @return array
	 */
	public function toArray()
	{
		return $this->convertObjectToArray($this);
	}

	/**
	 * @param $object
	 * @return array
	 */
	private function convertObjectToArray($object)
	{
		if (is_array($object) || is_object($object)) {
			$result = array();
			foreach ($object as $key => $value) {
				$result[$key] = $this->convertObjectToArray($value);
			}
			return $result;
		}
		return $object;
	}
}