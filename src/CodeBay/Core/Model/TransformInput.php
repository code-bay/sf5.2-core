<?php


namespace CodeBay\Core\Model;


class TransformInput
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var mixed
     */
    protected $data;

    /**
     * TransformInput constructor.
     * @param string $id
     * @param mixed $data
     */
    public function __construct(string $id, $data)
    {
        $this->id = $id;
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}