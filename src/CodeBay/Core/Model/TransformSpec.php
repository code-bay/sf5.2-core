<?php


namespace CodeBay\Core\Model;


class TransformSpec
{
    /**
     * @var StructurePath
     */
    protected $srcPath;

    /**
     * @var StructurePath
     */
    protected $dstPath;

    /**
     * @var string
     */
    protected $inputId;

    /**
     * @var array
     */
    protected $groupSpec;

    /**
     * TransformSpec constructor.
     * @param StructurePath $srcPath
     * @param StructurePath $dstPath
     * @param string $inputId
     * @param array $groupSpec
     */
    public function __construct(StructurePath $srcPath, StructurePath $dstPath, string $inputId, array $groupSpec)
    {
        $this->srcPath = $srcPath;
        $this->dstPath = $dstPath;
        $this->inputId = $inputId;
        $this->groupSpec = $groupSpec;
    }

    /**
     * @return bool
     */
    public function isRootGroupSpec() {
        if ($this->isGroupSpec()) {
            $groupSpec = $this->getGroupSpec();
            $hasIsRootKey = key_exists('isRoot', $groupSpec);
            return $hasIsRootKey && $groupSpec['isRoot'];
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isGroupSpec() {
        return $this->groupSpec !== null && $this->groupSpec !== [];
    }


    /**
     * @return StructurePath
     */
    public function getSrcPath(): StructurePath
    {
        return $this->srcPath;
    }

    /**
     * @return StructurePath
     */
    public function getDstPath(): StructurePath
    {
        return $this->dstPath;
    }

    /**
     * @return string
     */
    public function getInputId(): string
    {
        return $this->inputId;
    }

    /**
     * @return array
     */
    public function getGroupSpec(): array
    {
        return $this->groupSpec;
    }
}