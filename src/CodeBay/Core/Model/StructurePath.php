<?php


namespace CodeBay\Core\Model;


class StructurePath
{
    /**
     * @var string
     */
    protected $path;

    /**
     * StructurePath constructor.
     * @param string $path
     * @throws \Exception
     */
    public function __construct(string $path)
    {
        if ($path[0] !== '#' || str_ends_with($path, '.')) {
            throw new \Exception(sprintf('Invalid path %s', $path));
        }
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return bool
     */
    public function isRoot() {
        return $this->path === '#';
    }

    /**
     * @return array
     */
    public function getPathKeys() {
        $exploded = explode('.', $this->path);
        array_shift($exploded);
        return $exploded;
    }

}