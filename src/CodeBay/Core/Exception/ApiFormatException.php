<?php

namespace CodeBay\Core\Exception;

use Opis\JsonSchema\ValidationError;
use Throwable;

class ApiFormatException extends \Exception
{
    /**
     * @var ValidationError[]
     */
    protected $validationErrors;

    /**
     * ApiFormatException constructor.
     * @param $validationErrors
     * @param string $identifier
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($validationErrors, string $identifier = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($identifier, $code, $previous);
        $this->validationErrors = $validationErrors;
    }

    /**
     * @return ValidationError[]
     */
    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    /**
     * @return false|string
     */
    public function serializeErrorsToJson() {
        $jsonData = [];
        foreach ($this->validationErrors as $field => $validationErrorsByField) {
            foreach ($validationErrorsByField as $validationError) {
                $jsonData[] = [
                    'dataPointer' => $validationError->dataPointer(),
                    'keyword' => $validationError->keyword(),
                    'keywordArgs' => $validationError->keywordArgs(),
                    'parameter' => $field
                ];
            }
        }
        $jsonData = ['errors' => $jsonData];
        return json_encode($jsonData);
    }
}