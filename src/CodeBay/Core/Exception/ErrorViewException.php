<?php

namespace CodeBay\Core\Exception;

use Throwable;

class ErrorViewException extends \Exception
{
    /**
     * @var string|null
     */
    protected $template;

    /**
     * ErrorViewException constructor.
     * @param $template
     * @param string $identifier
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($template, string $identifier = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($identifier, $code, $previous);
        $this->template = $template;
    }

    /**
     * @return string|null
     */
    public function getTemplate(): ?string
    {
        return $this->template;
    }
}