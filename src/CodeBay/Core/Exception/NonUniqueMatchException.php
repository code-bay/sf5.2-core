<?php

namespace CodeBay\Core\Exception;

use Throwable;

class NonUniqueMatchException extends \Exception
{
    public function __construct(string $identifier = "", int $code = 0, Throwable $previous = null)
    {
        $message = sprintf('[%s] matched several results', print_r($identifier, true));
        parent::__construct($message, $code, $previous);
    }

}