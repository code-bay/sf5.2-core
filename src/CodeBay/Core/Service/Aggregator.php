<?php

namespace CodeBay\Core\Service;

use CodeBay\Core\Model\StructurePath;

/**
 * Class Aggregator
 * @package CodeBay\Hiram\CbApiGeneratorBundle\Service
 */
class Aggregator
{

    protected $groupDefinitions;
    protected $inputData;

    /**
     * @param $groupDefinitions
     * @param $inputData
     */
    public function __construct($groupDefinitions, $inputData)
    {
        $this->groupDefinitions = $groupDefinitions;
        $this->inputData = $inputData;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function aggregate() {
        $output = [];
        $path = new StructurePath('#');
        self::groupData($this->inputData, $this->groupDefinitions, $output, $path);
        return $output;
    }

    /**
     * @param $input
     * @param $groups
     * @param $output
     * @param StructurePath $path
     * @throws \Exception
     */
    public static function groupData($input, $groups, &$output, StructurePath $path) {
        $criteria = $groups['criteria'];
        $children = $groups['children'];
        $groupName = $groups['name'];
        $fields = $groups['fields'];
        $filters = key_exists('filters', $groups) ? $groups['filters'] : [];

        $groupData = [];
        if (!key_exists($groupName, $output)) {
            $groupData = self::getGroup($input, $criteria, $filters);
            $currentPath = new StructurePath(sprintf('%s.%s', $path->getPath(), $groupName));
            $reducedData = [];
            if (count($groupData) > 0) {
                $firstGroup = $groupData[0];
                foreach ($fields as $field) {
                    $fieldSrc = $field['src'];
                    $fieldDst = $fieldSrc;
                    if (key_exists('dst', $field)) {
                        $fieldDst = $field['dst'];
                    }
                    if (key_exists($fieldSrc, $firstGroup)) {
                        $reducedData[$fieldDst] = $firstGroup[$fieldSrc];
                    }
                }
            }
            ArrayTools::setValueAtPath($output, $currentPath, $groupData);
        }

        foreach ($groupData as $ind => $groupItem) {
            $currentSubPath = new StructurePath(sprintf('%s.%s.%s', $path->getPath(), $groupName, $ind));
            foreach ($children as $child) {
                self::groupData($groupItem, $child, $output, $currentSubPath);
            }
            // Reduce data
            $dataAtPath = ArrayTools::getValueAtPath($output, $currentSubPath);
            $reducedData = self::reduceData($dataAtPath, $fields);
            ArrayTools::setValueAtPath($output, $currentSubPath, $reducedData);
        }
    }

    /**
     * @param $dataToReduce
     * @param $fields
     * @return array
     */
    public static function reduceData($dataToReduce, $fields) {
        $reducedData = [];
        if (!key_exists(0, $dataToReduce)) {

            foreach ($fields as $field) {
                $fieldSrc = $field['src'];
                $fieldDst = $fieldSrc;
                if (key_exists('dst', $field)) {
                    $fieldDst = $field['dst'];
                }
                if (key_exists($fieldSrc, $dataToReduce)) {
                    $reducedData[$fieldDst] = $dataToReduce[$fieldSrc];
                }
            }

        } else {
            foreach ($dataToReduce as $key => $item) {
                if (is_numeric($key)) {
                    foreach ($fields as $field) {
                        $fieldSrc = $field['src'];
                        $fieldDst = $fieldSrc;
                        if (key_exists('dst', $field)) {
                            $fieldDst = $field['dst'];
                        }
                        if (key_exists($fieldSrc, $item)) {
                            $reducedData[$fieldDst] = $item[$fieldSrc];
                        }
                    }
                } else {
                    $reducedData[$key] = $item;
                }
            }
        }
        return $reducedData;
    }

    /**
     * @param $input
     * @param $criteria
     * @param array $filters
     * @return array
     */
    public static function getGroup($input, $criteria, $filters = []) {
        $groupedInput = [];
        $criteriaValues = [];
        $filteredItems = [];

        foreach ($input as $item) {
            // Item must validate all filters
            $validateAllFilters = true;
            foreach ($filters as $filter) {
                $filterSrc = $filter['src'];
                $filterOperation = $filter['operation'];
                if (!self::checkFilter($item, $filterSrc, $filterOperation)) {
                    $validateAllFilters = false;
                }
            }

            if ($validateAllFilters) $filteredItems[] = $item;
        }


        if ($criteria === null) {
            return $filteredItems;
        } else {
            foreach ($filteredItems as $item) {


                $criteriaValue = $item[$criteria];
                if (!in_array($criteriaValue, $criteriaValues)) {
                    $criteriaValues[] = $criteriaValue;
                }
                $i = 0;
                foreach ($criteriaValues as $index => $currentValue) {
                    if ($currentValue === $criteriaValue) {
                        $i = $index;
                    }
                }
                if (!key_exists($i, $groupedInput)) {
                    $groupedInput[$i] = [];

                }
                $groupedInput[$i][] = $item;
            }
        }

        return $groupedInput;
    }

    /**
     * @param $data
     * @param $src
     * @param $operation
     * @return bool
     */
    public static function checkFilter($data, $src, $operation) {
        if (!key_exists($src, $data)) return false;
        if ($operation === 'IS_TRUE') {
            return boolval($data[$src]);
        } else if ($operation === 'IS_FALSE') {
            return !boolval($data[$src]);
        }
        return false;
    }

}