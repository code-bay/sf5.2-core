<?php


namespace CodeBay\Core\Service;


use CodeBay\Core\Model\StructurePath;

/**
 * Class ArrayTools
 * @package CodeBay\Hiram\CbApiGeneratorBundle\Service
 */
class ArrayTools
{
    /**
     * @param $inputArray
     * @param StructurePath $path
     * @param $value
     */
    public static function setValueAtPath(&$inputArray, StructurePath $path, $value) {
        if ($path->isRoot()) {
            $inputArray = $value;
        } else {
            $pathKeys = $path->getPathKeys();
            foreach ($pathKeys as $ind => $key) {
                if (!key_exists($key, $inputArray)) {
                    $inputArray[$key] = [];
                }
                $isLast = $ind === count($pathKeys) - 1;
                if ($isLast) {
                    $inputArray[$key] = $value;
                } else {
                    $inputArray = &$inputArray[$key];
                }
            }
        }
    }

    /**
     * @param $inputArray
     * @param StructurePath $path
     * @return mixed|null
     */
    public static function getValueAtPath($inputArray, StructurePath $path) {

        if (!is_array($inputArray)) {
            if ($path->isRoot()) {
                return $inputArray;
            } else {
                return null;
            }
        }

        if ($path->isRoot()) {
            return $inputArray;
        }

        $keys = $path->getPathKeys();
        foreach ($keys as $i => $key) {
            if ($i >= count($keys) - 1) continue;
            if (!key_exists($key, $inputArray)) {
                $inputArray[$key] = [];
            }
            $inputArray = &$inputArray[$key];
        }
        $lastKey = $keys[count($keys) - 1];
        return key_exists($lastKey, $inputArray) ? $inputArray[$lastKey] : null;
    }

}