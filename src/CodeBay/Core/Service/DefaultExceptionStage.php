<?php

namespace CodeBay\Core\Service;

use CodeBay\Core\Exception\ApiFormatException;
use CodeBay\Core\Exception\ErrorViewException;
use CodeBay\Core\Pipeline\ApiActionStageInterface;
use CodeBay\Core\Pipeline\ExceptionAwareContext;
use CodeBay\Core\Tools\ResponseDataFormatter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class DefaultExceptionStage implements ApiActionStageInterface
{
    /**
     * @var Environment
     */
    private $twigRenderer;

    /**
     * DefaultResponseHandlerStage constructor.
     * @param Environment $twigRenderer
     */
    public function __construct(Environment $twigRenderer)
    {
        $this->twigRenderer = $twigRenderer;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param ExceptionAwareContext $context
     * @throws \Exception
     */
    public function processStage(Request $request, ExceptionAwareContext $context)
    {
        $response = $context->getResponse();
        $contextException = $context->getException();
        if ($contextException instanceof ErrorViewException) {
            $statusCode = $contextException->getCode();
            $context->setResponse(
                ResponseDataFormatter::buildResponseFromContext(
                    $request,
                    $statusCode,
                    $context,
                    $this->getTwigRenderer()
                )
            );
        } else if ($contextException instanceof ApiFormatException) {
            $errorsAsJson = $contextException->serializeErrorsToJson();
            $response->setContent($errorsAsJson);
            $response->headers->add(['Content-Type' => 'application/json']);
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            throw $contextException;
        }
    }

    /**
     * @return Environment
     */
    public function getTwigRenderer(): Environment
    {
        return $this->twigRenderer;
    }
}