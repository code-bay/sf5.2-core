<?php

namespace CodeBay\Core\Service;

use CodeBay\Core\Pipeline\ApiActionStageInterface;
use CodeBay\Core\Pipeline\ExceptionAwareContext;
use CodeBay\Core\Tools\ResponseDataFormatter;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

class DefaultResponseHandlerStage implements ApiActionStageInterface
{
    /**
     * @var Environment
     */
    private $twigRenderer;

    /**
     * DefaultResponseHandlerStage constructor.
     * @param Environment $twigRenderer
     */
    public function __construct(Environment $twigRenderer)
    {
        $this->twigRenderer = $twigRenderer;
    }

    /**
     * @param Request $request
     * @param ExceptionAwareContext $context
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function processStage(Request $request, ExceptionAwareContext $context)
    {
        $response = $context->getResponse();
        $statusCode = $response->getStatusCode();
        if ($statusCode) {
            $responseViewGetter = sprintf('getResponseCode%sView', $statusCode);
            if (method_exists($context, $responseViewGetter)) {
                $context->setResponse(
                    ResponseDataFormatter::buildResponseFromContext(
                        $request,
                        $statusCode,
                        $context,
                        $this->getTwigRenderer()
                    )
                );
            }
        }
    }

    /**
     * @return Environment
     */
    public function getTwigRenderer(): Environment
    {
        return $this->twigRenderer;
    }
}