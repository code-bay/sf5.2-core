<?php

namespace CodeBay\Core\Service;

use CodeBay\Core\Model\StructurePath;
use CodeBay\Core\Model\TransformInput;
use CodeBay\Core\Model\TransformSpec;
use PHPUnit\Runner\Exception;

/**
 * Class DataTransformer
 * @package CodeBay\Hiram\CbApiGeneratorBundle\Service
 */
class DataTransformer
{
    /**
     * @param $inputs
     * @param $specs
     * @return array
     * @throws \Exception
     */
    public static function transform($inputs, $specs)
    {
        $output = [];
        /** @var TransformSpec $spec */
        foreach ($specs as $spec) {
            $inputData = self::findInputById($inputs, $spec->getInputId());
            if (!$inputData) {
                throw new Exception(sprintf('No input found for id %s', $spec->getInputId()));
            }
            self::handleSingleSpec($inputData, $spec, $output);
        }

        return $output;
    }

    /**
     * @param TransformInput $input
     * @param TransformSpec $spec
     * @param $output
     * @throws \Exception
     */
    protected static function handleSingleSpec(TransformInput $input, TransformSpec $spec, &$output) {
        $srcData = ArrayTools::getValueAtPath($input->getData(), $spec->getSrcPath());
        if ($spec->isGroupSpec()) {
            $aggregator = new Aggregator($spec->getGroupSpec(), $srcData);
            $groupedData = $aggregator->aggregate();
            if ($spec->isRootGroupSpec()) {
                $firstKey = array_key_first($groupedData);
                ArrayTools::setValueAtPath($output, $spec->getDstPath(), $groupedData[$firstKey]);
            } else {
                foreach ($groupedData as $name => $groupedDatum) {
                    $newStructurePath = new StructurePath(sprintf('%s.%s', $spec->getDstPath()->getPath(), $name));
                    ArrayTools::setValueAtPath($output, $newStructurePath, $groupedDatum);
                }
            }

            // ArrayTools::setValueAtPath($output, $spec->getDstPath(), $groupedData);
        } else {
            ArrayTools::setValueAtPath($output, $spec->getDstPath(), $srcData);
        }
    }

    /**
     * @param TransformInput[] $inputs
     * @param $id
     * @return TransformInput|null
     */
    protected static function findInputById($inputs, $id)
    {
        foreach ($inputs as $input) {
            if ($input->getId() === $id) {
                return $input;
            }
        }
        return null;
    }

    /**
     * @param string|null $outputClass
     * @param $transformedData
     * @param $isJsonMappable
     * @param bool $isCollection
     * @return array|mixed|null
     * @throws \ReflectionException
     */
    public static function hydrateOutput(?string $outputClass, $transformedData, $isJsonMappable, $isCollection = false) {
        $hydrated = [];
        // Case one output is a JsonMappable
        if ($isJsonMappable) {
            if ($isCollection) {
                foreach ($transformedData as $transformedDatum) {
                    $hydrated[] = new $outputClass($transformedDatum);
                }
                return $hydrated;
            } else {
                return new $outputClass($transformedData);
            }
        }

        // Case two output is a scalar
        if ($outputClass === null) {
            if ($isCollection) {
                foreach ($transformedData as $transformedDatum) {
                    $firstKey = array_key_first($transformedDatum);
                    $hydrated[] = $transformedDatum[$firstKey];
                }
                return $hydrated;
            }
            return $transformedData;
        }

        // Case three : any other class
        if ($isCollection) {
            foreach ($transformedData as $transformedDatum) {
                $hydrated[] = self::hydrateSingleObject($outputClass, $transformedDatum);
            }
            return $hydrated;
        } else {
            return self::hydrateSingleObject($outputClass, $transformedData);
        }

    }

    /**
     * Hydrates an object using an associative array. Given classname must comprise setters matching with
     * arrays keys
     * @param string $className
     * @param $data
     * @return mixed
     * @throws \ReflectionException
     * @throws \Exception
     */
    protected static function hydrateSingleObject(string $className, $data) {
        $instance = new $className();

        foreach ($data as $fieldName => $datum) {
            $annotationType = self::getTypeNameFromAnnotation($className, $fieldName);
            $setter = sprintf('set%s%s', strtoupper($fieldName[0]), substr($fieldName, 1));
            if ($annotationType === '\DateTime') {
                $instance->$setter(new \DateTime($datum));
            } else {
                $instance->$setter($datum);
            }
        }
        return $instance;
    }

    /**
     * @param string $className
     * @param string $propertyName
     * @return string|null
     * @throws \ReflectionException
     */
    protected static function getTypeNameFromAnnotation(string $className, string $propertyName): ?string
    {
        $rp = new \ReflectionProperty($className, $propertyName);
        if (preg_match('/@var\s+([^\s]+)/', $rp->getDocComment(), $matches)) {
            return $matches[1];
        }
        return null;
    }
}