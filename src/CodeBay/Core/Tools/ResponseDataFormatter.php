<?php

namespace CodeBay\Core\Tools;

use CodeBay\Core\ArrayableInterface;
use CodeBay\Core\Pipeline\ExceptionAwareContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class ResponseDataFormatter
{
    /**
     * @param $responseData
     * @param $out
     */
    public static function formatResponseData($responseData, &$out) {
        if (is_array($responseData)) {
            foreach ($responseData as $key => $responseDatum) {
                if (!is_array($responseDatum) && !($responseDatum instanceof ArrayableInterface)) {
                    $out[$key] = $responseDatum;
                } else {
                    $out[] = [];
                    self::formatResponseData($responseDatum, $out[count($out) - 1]);
                }
            }
        } else if ($responseData instanceof ArrayableInterface) {
            $out = $responseData->toArray();
        }
    }

    /**
     * @param Request $request
     * @param $statusCode
     * @param ExceptionAwareContext $context
     * @param Environment $twigRenderer
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public static function buildResponseFromContext(
        Request $request,
        $statusCode,
        ExceptionAwareContext $context,
        Environment $twigRenderer
    ) {
        $response = new Response('', $statusCode);
        $responseViewGetter = sprintf('getResponseCode%sView', $statusCode);
        if (method_exists($context, $responseViewGetter)) {
            $responseData = $context->$responseViewGetter();
            $twigTemplate = $context->getTwigTemplate();
            $isJsonRequested = ResponseTypeDetector::isJsonRequested($request) || !$twigTemplate;
            $contentAsArray = [];
            ResponseDataFormatter::formatResponseData($responseData, $contentAsArray);

            if ($isJsonRequested) {
                $response->setContent(json_encode($contentAsArray));
                $response->headers->add(['Content-Type' => 'application/json']);
            } else {
                if ($twigTemplate) {
                    $data = [
                        'data' => $contentAsArray
                    ];
                    $rendered = $twigRenderer->render($twigTemplate, $data);
                    $response->setContent($rendered);
                }
            }
            $context->setResponse($response);
        }
        return $response;
    }
}