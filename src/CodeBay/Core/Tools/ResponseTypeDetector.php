<?php

namespace CodeBay\Core\Tools;

use Symfony\Component\HttpFoundation\Request;

class ResponseTypeDetector
{
    /**
     * @param Request $request
     * @return bool
     */
    public static function isJsonRequested(Request $request) {
        $headerJsonParam = $request->headers->get('Content-Type') === 'application/json';
        $queryJsonParam = $request->query->get('_format') === 'json';
        return $headerJsonParam || $queryJsonParam;
    }
}