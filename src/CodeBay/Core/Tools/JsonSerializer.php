<?php

namespace CodeBay\Core\Tools;


class JsonSerializer
{
    /**
     * @param $json
     * @return array
     */
    public static function decode($json) {
        $decodedJson = json_decode($json);
        return self::toAssocArray($decodedJson);
    }

    /**
     * @param $object
     * @return array
     */
    public static function toAssocArray($object) {
        $resultArray = [];
        if (is_object($object)) {
            $properties = array_keys(get_object_vars($object));
            if (count($properties) === 0) return $object;
            foreach ($properties as $property) {
                $resultArray[$property] = self::toAssocArray($object->$property);
            }
            return $resultArray;
        } else if (is_array($object)) {
            foreach ($object as $key => $item) {
                $resultArray[$key] = self::toAssocArray($item);
            }
            return $resultArray;
        }
        return $object;
    }
}