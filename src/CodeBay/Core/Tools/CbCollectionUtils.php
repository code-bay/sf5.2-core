<?php

namespace CodeBay\Core\Tools;

class CbCollectionUtils
{
    /**
     * Recursively exports a collection to an array
     *
     * @param mixed $collection
     * @return array
     */
    public static function toArrayRecursive($collection) {
        $arr = $collection;
        if (is_object($collection) && method_exists($collection, 'toArray')) {
            $arr = $collection->toArray();
        }

        if (is_array($arr)) {
            return array_map(function ($v) {
                if (is_object($v) && method_exists($v, 'toArray')) {
                    return static::toArrayRecursive($v);
                }
                return $v;
            }, $arr);
        } else {
            return $arr;
        }
    }
}