<?php

namespace CodeBay\Core;

use CodeBay\Core\JsonSchema\JsonSchemaMapper;
use CodeBay\Core\Tools\JsonSerializer;
use DocBlockReader\Reader;

class JsonMappable implements ArrayableInterface
{
    /**
     * JsonMappable constructor.
     * @param array|null $mixed
     * @throws \Exception
     */
	public function __construct(array $mixed = null)
	{
        $jsonSchema = $this->getJsonSchemaAnnotation();
        if ($jsonSchema) {
            return $this->hydrateWithJsonSchema($mixed);
        } else {
            return $this->hydrateLegacy($mixed);
        }
	}

    /**
     * @param array|null $mixed
     * @return mixed
     * @throws \Exception
     */
	protected function hydrateWithJsonSchema(array $mixed = null) {
        $asJson = json_encode($this->convertObjectToArray($mixed));
        if ($asJson) {
            $jsonPayload = $asJson;
            $jsonSchema = json_encode($this->getJsonSchemaAnnotation());
            $package = $this->getPackageAnnotation();
            $definitionNamespace = $this->getDefinitionNamespaceAnnotation();
            $mapper = new JsonSchemaMapper();
            return $mapper->mapObject($this, $jsonSchema, $jsonPayload, $package, $definitionNamespace);
        }
        return null;
    }

    /**
     * @param array|null $mixed
     * @return object
     * @throws \JsonMapper_Exception
     */
    protected function hydrateLegacy(array $mixed = null) {
        if ($mixed !== null) {
            $asJson = json_encode($this->convertObjectToArray($mixed));
            if ($asJson) {
                $jsonPayload = json_decode($asJson);
                $mapper = new \JsonMapper();
                $mapper->bStrictNullTypes = false;
                if (is_object($jsonPayload)) {
                    return $mapper->map($jsonPayload, $this);
                }
            }
        }
        return null;
    }

    /**
     * @return array
     * @throws \Exception
     */
	protected function getJsonSchemaAnnotation() {
        $className = get_class($this);
        $reader = new Reader($className);
        $jsonSchema = $reader->getParameter('jsonSchema');
        return JsonSerializer::decode(substr($jsonSchema, 2));
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    protected function getPackageAnnotation() {
        $className = get_class($this);
        $reader = new Reader($className);
        return $reader->getParameter('package');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    protected function getDefinitionNamespaceAnnotation() {
        $className = get_class($this);
        $reader = new Reader($className);
        return $reader->getParameter('definitionNamespace');
    }

    /**
	 * @return array
	 */
	public function toArray()
	{
		return $this->convertObjectToArray($this);
	}

	/**
	 * @param $object
	 * @return array|\StdClass
	 */
	private function convertObjectToArray($object)
	{
		if (is_array($object) || is_object($object)) {
			$result = array();
			foreach ($object as $key => $value) {
			    $newValue = $this->convertObjectToArray($value);
			    if ($key === 'ref') $key = '$ref';
			    if ($newValue !== null) {
                    $result[$key] = $newValue;
                }
			}
			if (empty($result) && is_object($object)) {
			    return new \StdClass();
            }
			return $result;
		}
		return $object;
	}
}